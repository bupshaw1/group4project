USE [master]
GO
/****** Object:  Database [CS6232-g4]    Script Date: 3/31/2017 10:30:31 PM ******/
IF  EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'CS6232-g4')
ALTER DATABASE [CS6232-g4] SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
GO

DROP DATABASE [CS6232-g4]
GO

CREATE DATABASE [CS6232-g4]
GO

ALTER DATABASE [CS6232-g4] MODIFY FILE
( NAME = N'CS6232-g4', SIZE = 5184KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 GO

 ALTER DATABASE [CS6232-g4] MODIFY FILE
( NAME = N'CS6232-g4_log', SIZE = 832KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO

ALTER DATABASE [CS6232-g4] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CS6232-g4].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CS6232-g4] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CS6232-g4] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CS6232-g4] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CS6232-g4] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CS6232-g4] SET ARITHABORT OFF 
GO
ALTER DATABASE [CS6232-g4] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [CS6232-g4] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [CS6232-g4] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CS6232-g4] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CS6232-g4] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CS6232-g4] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CS6232-g4] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CS6232-g4] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CS6232-g4] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CS6232-g4] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CS6232-g4] SET  ENABLE_BROKER 
GO
ALTER DATABASE [CS6232-g4] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CS6232-g4] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CS6232-g4] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CS6232-g4] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CS6232-g4] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CS6232-g4] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CS6232-g4] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CS6232-g4] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [CS6232-g4] SET  MULTI_USER 
GO
ALTER DATABASE [CS6232-g4] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CS6232-g4] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CS6232-g4] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CS6232-g4] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [CS6232-g4]
GO
/****** Object:  Table [dbo].[administrator]    Script Date: 4/17/2017 10:49:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[administrator](
	[adminID] [int] IDENTITY(1,1) NOT NULL,
	[personID] [int] NOT NULL,
	[userID] [int] NOT NULL,
 CONSTRAINT [PK_administrator] PRIMARY KEY CLUSTERED 
(
	[adminID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[appointment]    Script Date: 4/17/2017 10:49:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[appointment](
	[apptID] [int] IDENTITY(1,1) NOT NULL,
	[apptDate] [date] NOT NULL,
	[apptTime] [time](7) NOT NULL,
	[reasonForVisit] [varchar](100) NOT NULL,
	[patientID] [int] NOT NULL,
	[doctorID] [int] NOT NULL,
 CONSTRAINT [PK_appointment] PRIMARY KEY CLUSTERED 
(
	[apptID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[doctor]    Script Date: 4/17/2017 10:49:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[doctor](
	[doctorID] [int] IDENTITY(1,1) NOT NULL,
	[personID] [int] NOT NULL,
	[userID] [int] NOT NULL,
 CONSTRAINT [PK_doctor] PRIMARY KEY CLUSTERED 
(
	[doctorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[dr_specialties]    Script Date: 4/17/2017 10:49:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dr_specialties](
	[drSpecialityID] [int] IDENTITY(1,1) NOT NULL,
	[doctorID] [int] NOT NULL,
	[specialtyID] [int] NOT NULL,
 CONSTRAINT [PK_dr_specialties] PRIMARY KEY CLUSTERED 
(
	[drSpecialityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[nurse]    Script Date: 4/17/2017 10:49:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[nurse](
	[nurseID] [int] IDENTITY(1,1) NOT NULL,
	[personID] [int] NOT NULL,
	[userID] [int] NOT NULL,
 CONSTRAINT [PK_nurse] PRIMARY KEY CLUSTERED 
(
	[nurseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[patient]    Script Date: 4/17/2017 10:49:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[patient](
	[patientID] [int] IDENTITY(1,1) NOT NULL,
	[personID] [int] NOT NULL,
 CONSTRAINT [PK_patient] PRIMARY KEY CLUSTERED 
(
	[patientID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[person]    Script Date: 4/17/2017 10:49:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[person](
	[personID] [int] IDENTITY(1,1) NOT NULL,
	[fname] [varchar](45) NOT NULL,
	[lname] [varchar](45) NOT NULL,
	[dob] [date] NOT NULL,
	[address] [varchar](45) NOT NULL,
	[city] [varchar](45) NOT NULL,
	[state] [char](2) NOT NULL,
	[zip] [char](50) NOT NULL,
	[phone] [varchar](15) NOT NULL,
	[gender] [char](1) NOT NULL,
	[ssn] [char](9) NOT NULL,
 CONSTRAINT [PK_person] PRIMARY KEY CLUSTERED 
(
	[personID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[prog_user]    Script Date: 4/17/2017 10:49:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[prog_user](
	[userID] [int] IDENTITY(1,1) NOT NULL,
	[password] [varchar](200) NOT NULL,
	[loginID] [varchar](35) NULL,
 CONSTRAINT [PK_user] PRIMARY KEY CLUSTERED 
(
	[userID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[specialty]    Script Date: 4/17/2017 10:49:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[specialty](
	[specialtyID] [int] IDENTITY(1,1) NOT NULL,
	[specialtyName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_specialty] PRIMARY KEY CLUSTERED 
(
	[specialtyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[stateList]    Script Date: 4/17/2017 10:49:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[stateList](
	[stateID] [nvarchar](255) NULL,
	[stateName] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[test]    Script Date: 4/17/2017 10:49:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[test](
	[testID] [int] IDENTITY(1,1) NOT NULL,
	[testName] [varchar](100) NOT NULL,
 CONSTRAINT [PK_test] PRIMARY KEY CLUSTERED 
(
	[testID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tests_performed]    Script Date: 4/17/2017 10:49:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tests_performed](
	[testPerformedID] [int] IDENTITY(1,1) NOT NULL,
	[visitID] [int] NOT NULL,
	[testID] [int] NOT NULL,
	[resultsNormal] [bit],
	[datePerformed] [date],
 CONSTRAINT [PK_tests_performed] PRIMARY KEY CLUSTERED 
(
	[testPerformedID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[visit]    Script Date: 4/17/2017 10:49:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[visit](
	[visitID] [int] IDENTITY(1,1) NOT NULL,
	[apptID] [int] NOT NULL,
	[bpSystolic] [int] NOT NULL,
	[bpDiastolic] [int] NOT NULL,
	[temperature] [float] NOT NULL,
	[pulse] [int] NOT NULL,
	[nurseID] [int] NOT NULL,
	[symptoms] [varchar](200) NOT NULL,
	[diagnosis] [varchar](200) NOT NULL,
 CONSTRAINT [PK_visit] PRIMARY KEY CLUSTERED 
(
	[visitID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[administrator] ON 

INSERT [dbo].[administrator] ([adminID], [personID], [userID]) VALUES (1, 8, 7)
INSERT [dbo].[administrator] ([adminID], [personID], [userID]) VALUES (2, 9, 6)
SET IDENTITY_INSERT [dbo].[administrator] OFF
SET IDENTITY_INSERT [dbo].[appointment] ON 

INSERT [dbo].[appointment] ([apptID], [apptDate], [apptTime], [reasonForVisit], [patientID], [doctorID]) VALUES (1, CAST(0xA33C0B00 AS Date), CAST(0x070040230E430000 AS Time), N'Sore throat', 1, 1)
INSERT [dbo].[appointment] ([apptID], [apptDate], [apptTime], [reasonForVisit], [patientID], [doctorID]) VALUES (2, CAST(0xA43C0B00 AS Date), CAST(0x07005A9426450000 AS Time), N'Stiff neck', 5, 2)
INSERT [dbo].[appointment] ([apptID], [apptDate], [apptTime], [reasonForVisit], [patientID], [doctorID]) VALUES (3, CAST(0xA63C0B00 AS Date), CAST(0x0700A8E76F4B0000 AS Time), N'Sports physical', 12, 1)
INSERT [dbo].[appointment] ([apptID], [apptDate], [apptTime], [reasonForVisit], [patientID], [doctorID]) VALUES (4, CAST(0xA33C0B00 AS Date), CAST(0x070040230E430000 AS Time), N'Pregnant', 2, 1)
INSERT [dbo].[appointment] ([apptID], [apptDate], [apptTime], [reasonForVisit], [patientID], [doctorID]) VALUES (5, CAST(0xA43C0B00 AS Date), CAST(0x07005A9426450000 AS Time), N'Bloody Knuckles', 3, 2)
INSERT [dbo].[appointment] ([apptID], [apptDate], [apptTime], [reasonForVisit], [patientID], [doctorID]) VALUES (6, CAST(0xA63C0B00 AS Date), CAST(0x0700A8E76F4B0000 AS Time), N'Laughing Uncontrollably', 3, 1)
SET IDENTITY_INSERT [dbo].[appointment] OFF
SET IDENTITY_INSERT [dbo].[doctor] ON 

INSERT [dbo].[doctor] ([doctorID], [personID], [userID]) VALUES (1, 3, 3)
INSERT [dbo].[doctor] ([doctorID], [personID], [userID]) VALUES (2, 5, 5)
SET IDENTITY_INSERT [dbo].[doctor] OFF
SET IDENTITY_INSERT [dbo].[dr_specialties] ON 

INSERT [dbo].[dr_specialties] ([drSpecialityID], [doctorID], [specialtyID]) VALUES (1, 1, 1)
INSERT [dbo].[dr_specialties] ([drSpecialityID], [doctorID], [specialtyID]) VALUES (2, 2, 2)
SET IDENTITY_INSERT [dbo].[dr_specialties] OFF
SET IDENTITY_INSERT [dbo].[nurse] ON 

INSERT [dbo].[nurse] ([nurseID], [personID], [userID]) VALUES (1, 1, 1)
INSERT [dbo].[nurse] ([nurseID], [personID], [userID]) VALUES (2, 2, 2)
SET IDENTITY_INSERT [dbo].[nurse] OFF
SET IDENTITY_INSERT [dbo].[patient] ON 

INSERT [dbo].[patient] ([patientID], [personID]) VALUES (1, 6)
INSERT [dbo].[patient] ([patientID], [personID]) VALUES (2, 7)
INSERT [dbo].[patient] ([patientID], [personID]) VALUES (3, 10)
INSERT [dbo].[patient] ([patientID], [personID]) VALUES (4, 11)
INSERT [dbo].[patient] ([patientID], [personID]) VALUES (5, 12)
INSERT [dbo].[patient] ([patientID], [personID]) VALUES (6, 13)
INSERT [dbo].[patient] ([patientID], [personID]) VALUES (7, 14)
INSERT [dbo].[patient] ([patientID], [personID]) VALUES (8, 15)
INSERT [dbo].[patient] ([patientID], [personID]) VALUES (9, 16)
INSERT [dbo].[patient] ([patientID], [personID]) VALUES (10, 17)
INSERT [dbo].[patient] ([patientID], [personID]) VALUES (11, 18)
INSERT [dbo].[patient] ([patientID], [personID]) VALUES (12, 19)
SET IDENTITY_INSERT [dbo].[patient] OFF
SET IDENTITY_INSERT [dbo].[person] ON 

INSERT [dbo].[person] ([personID], [fname], [lname], [dob], [address], [city], [state], [zip], [phone], [gender], [ssn]) VALUES (1, N'Florence', N'Nightingale', CAST(0x2EF10A00 AS Date), N'123 Any Street', N'Carrollton', N'GA', N'30118                                             ', N'(678)-839-5000', N'F', N'123456789')
INSERT [dbo].[person] ([personID], [fname], [lname], [dob], [address], [city], [state], [zip], [phone], [gender], [ssn]) VALUES (2, N'Clara', N'Barton', CAST(0x520A0B00 AS Date), N'456 Any Street', N'Carrollton', N'GA', N'30118                                             ', N'(678) 839-5001', N'F', N'234567890')
INSERT [dbo].[person] ([personID], [fname], [lname], [dob], [address], [city], [state], [zip], [phone], [gender], [ssn]) VALUES (3, N'Joseph', N'Lister', CAST(0x53FC0A00 AS Date), N'789 Any Street', N'Carrollton', N'GA', N'30118                                             ', N'(678) 839-5002', N'M', N'345678901')
INSERT [dbo].[person] ([personID], [fname], [lname], [dob], [address], [city], [state], [zip], [phone], [gender], [ssn]) VALUES (5, N'Jack', N'Kevorkian', CAST(0xD5F70A00 AS Date), N'123 New Street', N'Carrollton', N'GA', N'30118                                             ', N'(678) 839-5004', N'M', N'456789012')
INSERT [dbo].[person] ([personID], [fname], [lname], [dob], [address], [city], [state], [zip], [phone], [gender], [ssn]) VALUES (6, N'Jane', N'Doe', CAST(0x3AF90A00 AS Date), N'456 New Street', N'Carrollton', N'GA', N'30118                                             ', N'(678) 839-5004', N'F', N'567890123')
INSERT [dbo].[person] ([personID], [fname], [lname], [dob], [address], [city], [state], [zip], [phone], [gender], [ssn]) VALUES (7, N'John', N'Doe', CAST(0x33E00A00 AS Date), N'789 New Street', N'Carrollton', N'GA', N'30118                                             ', N'(678) 839-5005', N'M', N'678901234')
INSERT [dbo].[person] ([personID], [fname], [lname], [dob], [address], [city], [state], [zip], [phone], [gender], [ssn]) VALUES (8, N'William', N'Shatner', CAST(0x52D90A00 AS Date), N'123 Gold Street', N'Hollywood', N'CA', N'12345                                             ', N'(555) 555-121', N'M', N'789012345')
INSERT [dbo].[person] ([personID], [fname], [lname], [dob], [address], [city], [state], [zip], [phone], [gender], [ssn]) VALUES (9, N'Leonard', N'Nimoy', CAST(0x5F0D0B00 AS Date), N'456 Gold Street', N'Hollywood', N'CA', N'12345                                             ', N'(555) 555-1313', N'M', N'890123456')
INSERT [dbo].[person] ([personID], [fname], [lname], [dob], [address], [city], [state], [zip], [phone], [gender], [ssn]) VALUES (10, N'John', N'Doe', CAST(0x81FB0A00 AS Date), N'123 New Street', N'Atlanta', N'GA', N'30301                                             ', N'(678) 123-4567', N'M', N'167890123')
INSERT [dbo].[person] ([personID], [fname], [lname], [dob], [address], [city], [state], [zip], [phone], [gender], [ssn]) VALUES (11, N'Roger', N'Doe', CAST(0xEBF70A00 AS Date), N'234 New Street', N'Atlanta', N'GA', N'30301                                             ', N'(678) 234-5678', N'M', N'267890123')
INSERT [dbo].[person] ([personID], [fname], [lname], [dob], [address], [city], [state], [zip], [phone], [gender], [ssn]) VALUES (12, N'Craig', N'Doe', CAST(0x4E1D0B00 AS Date), N'345 New Street', N'Atlanta', N'GA', N'30301                                             ', N'(678) 345-6789', N'M', N'367890123')
INSERT [dbo].[person] ([personID], [fname], [lname], [dob], [address], [city], [state], [zip], [phone], [gender], [ssn]) VALUES (13, N'Daniel', N'Doe', CAST(0xCB1A0B00 AS Date), N'567 New Street', N'Atlanta', N'GA', N'30301                                             ', N'(678) 456-7890', N'M', N'467890123')
INSERT [dbo].[person] ([personID], [fname], [lname], [dob], [address], [city], [state], [zip], [phone], [gender], [ssn]) VALUES (14, N'Patrick', N'Doe', CAST(0x860D0B00 AS Date), N'678 New Street', N'Atlanta', N'GA', N'30301                                             ', N'(678) 567-8901', N'M', N'667890123')
INSERT [dbo].[person] ([personID], [fname], [lname], [dob], [address], [city], [state], [zip], [phone], [gender], [ssn]) VALUES (15, N'Jill', N'Doe', CAST(0x3AF90A00 AS Date), N'890 New Street', N'Carrollton', N'GA', N'30118                                             ', N'(678) 678-9012', N'F', N'767890123')
INSERT [dbo].[person] ([personID], [fname], [lname], [dob], [address], [city], [state], [zip], [phone], [gender], [ssn]) VALUES (16, N'Hannah', N'Doe', CAST(0xB3120B00 AS Date), N'901 New Street', N'Carrollton', N'GA', N'30118                                             ', N'(678) 789-0123', N'F', N'867890123')
INSERT [dbo].[person] ([personID], [fname], [lname], [dob], [address], [city], [state], [zip], [phone], [gender], [ssn]) VALUES (17, N'Claire', N'Doe', CAST(0x89040B00 AS Date), N'012 New Street', N'Carrollton', N'GA', N'30118                                             ', N'(678) 890-1234', N'F', N'967890123')
INSERT [dbo].[person] ([personID], [fname], [lname], [dob], [address], [city], [state], [zip], [phone], [gender], [ssn]) VALUES (18, N'Wilma', N'Doe', CAST(0x2E360B00 AS Date), N'011 New Street', N'Carrollton', N'GA', N'30118                                             ', N'(678) 901-2345', N'F', N'067890123')
INSERT [dbo].[person] ([personID], [fname], [lname], [dob], [address], [city], [state], [zip], [phone], [gender], [ssn]) VALUES (19, N'Tiffany', N'Doe', CAST(0x84240B00 AS Date), N'022 New Street', N'Carrollton', N'GA', N'30118                                             ', N'(678) 012-3456', N'F', N'127890123')
SET IDENTITY_INSERT [dbo].[person] OFF
SET IDENTITY_INSERT [dbo].[prog_user] ON 

INSERT [dbo].[prog_user] ([userID], [password], [loginID]) VALUES (1, N'fc0iUkg331qk3V8HY6MWvQ==', N'fnight')
INSERT [dbo].[prog_user] ([userID], [password], [loginID]) VALUES (2, N'fc0iUkg331qk3V8HY6MWvQ==', N'cbarton')
INSERT [dbo].[prog_user] ([userID], [password], [loginID]) VALUES (3, N'fc0iUkg331qk3V8HY6MWvQ==', N'jlister')
INSERT [dbo].[prog_user] ([userID], [password], [loginID]) VALUES (5, N'fc0iUkg331qk3V8HY6MWvQ==', N'jkevork')
INSERT [dbo].[prog_user] ([userID], [password], [loginID]) VALUES (6, N'fc0iUkg331qk3V8HY6MWvQ==', N'lnimoy')
INSERT [dbo].[prog_user] ([userID], [password], [loginID]) VALUES (7, N'fc0iUkg331qk3V8HY6MWvQ==', N'wshat')
SET IDENTITY_INSERT [dbo].[prog_user] OFF
SET IDENTITY_INSERT [dbo].[specialty] ON 

INSERT [dbo].[specialty] ([specialtyID], [specialtyName]) VALUES (1, N'Cardiology')
INSERT [dbo].[specialty] ([specialtyID], [specialtyName]) VALUES (2, N'Neurology')
INSERT [dbo].[specialty] ([specialtyID], [specialtyName]) VALUES (3, N'Ear Nose Throat')
SET IDENTITY_INSERT [dbo].[specialty] OFF
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'AL', N'Alabama')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'AK', N'Alaska')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'AZ', N'Arizona')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'AR', N'Arkansas')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'CA', N'California')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'CO', N'Colorado')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'CT', N'Connecticut')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'DE', N'Delaware')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'FL', N'Florida')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'GA', N'Georgia')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'HI', N'Hawaii')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'ID', N'Idaho')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'IL', N'Illinois')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'IN', N'Indiana')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'IA', N'Iowa')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'KS', N'Kansas')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'KY', N'Kentucky')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'LA', N'Louisiana')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'ME', N'Maine')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'MD', N'Maryland')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'MA', N'Massachusetts')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'MI', N'Michigan')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'MN', N'Minnesota')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'MS', N'Mississippi')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'MO', N'Missouri')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'MT', N'Montana')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'NE', N'Nebraska')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'NV', N'Nevada')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'NH', N'New Hampshire')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'NJ', N'New Jersey')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'NM', N'New Mexico')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'NY', N'New York')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'NC', N'North Carolina')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'ND', N'North Dakota')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'OH', N'Ohio')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'OK', N'Oklahoma')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'OR', N'Oregon')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'PA', N'Pennsylvania')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'RI', N'Rhode Island')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'SC', N'South Carolina')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'SD', N'South Dakota')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'TN', N'Tennessee')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'TX', N'Texas')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'UT', N'Utah')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'VT', N'Vermont')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'VA', N'Virginia')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'WA', N'Washington')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'WV', N'West Virginia')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'WI', N'Wisconsin')
INSERT [dbo].[stateList] ([stateID], [stateName]) VALUES (N'WY', N'Wyoming')
SET IDENTITY_INSERT [dbo].[test] ON 

INSERT [dbo].[test] ([testID], [testName]) VALUES (1, N'WBC')
INSERT [dbo].[test] ([testID], [testName]) VALUES (2, N'RBC')
INSERT [dbo].[test] ([testID], [testName]) VALUES (3, N'Urinalysis')
INSERT [dbo].[test] ([testID], [testName]) VALUES (4, N'Hemoglobin')
INSERT [dbo].[test] ([testID], [testName]) VALUES (5, N'Glucose')
SET IDENTITY_INSERT [dbo].[test] OFF
SET IDENTITY_INSERT [dbo].[tests_performed] ON 

INSERT [dbo].[tests_performed] ([testPerformedID], [visitID], [testID], [resultsNormal], [datePerformed]) VALUES (1, 1, 1, 1, CAST(0xA33C0B00 AS Date))
INSERT [dbo].[tests_performed] ([testPerformedID], [visitID], [testID], [resultsNormal], [datePerformed]) VALUES (2, 1, 3, 0, CAST(0xA33C0B00 AS Date))
INSERT [dbo].[tests_performed] ([testPerformedID], [visitID], [testID], [resultsNormal], [datePerformed]) VALUES (3, 1, 4, 1, CAST(0xA33C0B00 AS Date))
INSERT [dbo].[tests_performed] ([testPerformedID], [visitID], [testID], [resultsNormal], [datePerformed]) VALUES (4, 2, 1, 0, CAST(0xA43C0B00 AS Date))
INSERT [dbo].[tests_performed] ([testPerformedID], [visitID], [testID], [resultsNormal], [datePerformed]) VALUES (5, 2, 3, 1, CAST(0xA43C0B00 AS Date))
INSERT [dbo].[tests_performed] ([testPerformedID], [visitID], [testID], [resultsNormal], [datePerformed]) VALUES (6, 2, 4, 0, CAST(0xA43C0B00 AS Date))
INSERT [dbo].[tests_performed] ([testPerformedID], [visitID], [testID], [resultsNormal], [datePerformed]) VALUES (7, 3, 1, 1, CAST(0xA43C0B00 AS Date))
INSERT [dbo].[tests_performed] ([testPerformedID], [visitID], [testID], [resultsNormal], [datePerformed]) VALUES (8, 3, 3, 0, CAST(0xA43C0B00 AS Date))
INSERT [dbo].[tests_performed] ([testPerformedID], [visitID], [testID], [resultsNormal], [datePerformed]) VALUES (9, 3, 4, 1, CAST(0xA43C0B00 AS Date))
SET IDENTITY_INSERT [dbo].[tests_performed] OFF
SET IDENTITY_INSERT [dbo].[visit] ON 

INSERT [dbo].[visit] ([visitID], [apptID], [bpSystolic], [bpDiastolic], [temperature], [pulse], [nurseID], [symptoms], [diagnosis]) VALUES (1, 1, 120, 80, 100.5, 120, 1, N'sore scratchy throat', N'Initial diagnosis')
INSERT [dbo].[visit] ([visitID], [apptID], [bpSystolic], [bpDiastolic], [temperature], [pulse], [nurseID], [symptoms], [diagnosis]) VALUES (2, 2, 130, 79, 98.6, 100, 2, N'Sore, stiff neck', N'initial diagnosis')
INSERT [dbo].[visit] ([visitID], [apptID], [bpSystolic], [bpDiastolic], [temperature], [pulse], [nurseID], [symptoms], [diagnosis]) VALUES (3, 3, 121, 81, 98.6, 120, 1, N'Annual sports physical', N'Physical')
SET IDENTITY_INSERT [dbo].[visit] OFF
ALTER TABLE [dbo].[visit] ADD  CONSTRAINT [DF_visit_diagnosis]  DEFAULT ('Replace with diagnosis') FOR [diagnosis]
GO
ALTER TABLE [dbo].[administrator]  WITH CHECK ADD  CONSTRAINT [FK_administrator_person] FOREIGN KEY([personID])
REFERENCES [dbo].[person] ([personID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[administrator] CHECK CONSTRAINT [FK_administrator_person]
GO
ALTER TABLE [dbo].[administrator]  WITH CHECK ADD  CONSTRAINT [FK_administrator_progUser] FOREIGN KEY([userID])
REFERENCES [dbo].[prog_user] ([userID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[administrator] CHECK CONSTRAINT [FK_administrator_progUser]
GO
ALTER TABLE [dbo].[appointment]  WITH CHECK ADD  CONSTRAINT [FK_appointment_doctor] FOREIGN KEY([doctorID])
REFERENCES [dbo].[doctor] ([doctorID])
GO
ALTER TABLE [dbo].[appointment] CHECK CONSTRAINT [FK_appointment_doctor]
GO
ALTER TABLE [dbo].[appointment]  WITH CHECK ADD  CONSTRAINT [FK_appointment_patient] FOREIGN KEY([patientID])
REFERENCES [dbo].[patient] ([patientID])
GO
ALTER TABLE [dbo].[appointment] CHECK CONSTRAINT [FK_appointment_patient]
GO
ALTER TABLE [dbo].[doctor]  WITH CHECK ADD  CONSTRAINT [FK_doctor_person] FOREIGN KEY([personID])
REFERENCES [dbo].[person] ([personID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[doctor] CHECK CONSTRAINT [FK_doctor_person]
GO
ALTER TABLE [dbo].[doctor]  WITH CHECK ADD  CONSTRAINT [FK_doctor_progUser] FOREIGN KEY([userID])
REFERENCES [dbo].[prog_user] ([userID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[doctor] CHECK CONSTRAINT [FK_doctor_progUser]
GO
ALTER TABLE [dbo].[dr_specialties]  WITH CHECK ADD  CONSTRAINT [FK_drSpecialties_doctor] FOREIGN KEY([doctorID])
REFERENCES [dbo].[doctor] ([doctorID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[dr_specialties] CHECK CONSTRAINT [FK_drSpecialties_doctor]
GO
ALTER TABLE [dbo].[dr_specialties]  WITH CHECK ADD  CONSTRAINT [FK_drSpecialties_speciality] FOREIGN KEY([specialtyID])
REFERENCES [dbo].[specialty] ([specialtyID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[dr_specialties] CHECK CONSTRAINT [FK_drSpecialties_speciality]
GO
ALTER TABLE [dbo].[nurse]  WITH CHECK ADD  CONSTRAINT [FK_nurse_person] FOREIGN KEY([personID])
REFERENCES [dbo].[person] ([personID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[nurse] CHECK CONSTRAINT [FK_nurse_person]
GO
ALTER TABLE [dbo].[nurse]  WITH CHECK ADD  CONSTRAINT [FK_nurse_progUser] FOREIGN KEY([userID])
REFERENCES [dbo].[prog_user] ([userID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[nurse] CHECK CONSTRAINT [FK_nurse_progUser]
GO
ALTER TABLE [dbo].[patient]  WITH CHECK ADD  CONSTRAINT [FK_patient_person] FOREIGN KEY([personID])
REFERENCES [dbo].[person] ([personID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[patient] CHECK CONSTRAINT [FK_patient_person]
GO
ALTER TABLE [dbo].[tests_performed]  WITH CHECK ADD  CONSTRAINT [FK_testsPerformed_test] FOREIGN KEY([testID])
REFERENCES [dbo].[test] ([testID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[tests_performed] CHECK CONSTRAINT [FK_testsPerformed_test]
GO
ALTER TABLE [dbo].[tests_performed]  WITH CHECK ADD  CONSTRAINT [FK_testsPerformed_visit] FOREIGN KEY([visitID])
REFERENCES [dbo].[visit] ([visitID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[tests_performed] CHECK CONSTRAINT [FK_testsPerformed_visit]
GO
ALTER TABLE [dbo].[visit]  WITH CHECK ADD  CONSTRAINT [FK_visit_appointment] FOREIGN KEY([apptID])
REFERENCES [dbo].[appointment] ([apptID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[visit] CHECK CONSTRAINT [FK_visit_appointment]
GO
ALTER TABLE [dbo].[visit]  WITH CHECK ADD  CONSTRAINT [FK_visit_nurse] FOREIGN KEY([nurseID])
REFERENCES [dbo].[nurse] ([nurseID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[visit] CHECK CONSTRAINT [FK_visit_nurse]
GO
USE [master]
GO
ALTER DATABASE [CS6232-g4] SET  READ_WRITE 
GO

USE [CS6232-g4];  
GO  

IF  EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'dbo.sp_mostPerformedTests')
DROP PROCEDURE sp_mostPerformedTestDuringDates
GO

CREATE PROCEDURE dbo.sp_mostPerformedTestsDuringDates   
    @startDate varchar(10),   
    @endDate varchar(10)   
AS   

SELECT performed.testID AS test_code, test.testName AS test_name, COUNT(performed.testID) AS total_times_performed, 
(SELECT COUNT(performed.testPerformedID)
FROM dbo.tests_performed performed
WHERE performed.datePerformed BETWEEN @startDate AND @endDate HAVING (SELECT COUNT(performed.testID)) >= 2) AS all_tests_total,
CAST(COUNT(performed.testID / performed.testPerformedID) * 10 AS DECIMAL(10,2)) AS percent_of_total,
SUM(CASE WHEN performed.resultsNormal = 0 THEN 1 ELSE 0 END) AS 'normal_test_results',
SUM(CASE WHEN performed.resultsNormal = 1 THEN 1 ELSE 0 END) AS 'abnormal_test_results',
CAST(SUM(case when datediff(DAY, person.dob, performed.datePerformed) / 365 BETWEEN 18 and 29.99 then 1 else 0 end) * 100 AS DECIMAL(10, 2)) / COUNT(performed.testID) AS 'patients_18_to_29',
CAST(SUM(case when datediff(DAY, person.dob, performed.datePerformed) / 365 BETWEEN 0 and 17.99 then 1 when datediff(DAY, person.dob, performed.datePerformed) / 365 BETWEEN 30 and 200 then 1 else 0 end) * 100 AS DECIMAL(10, 2)) / COUNT(performed.testID) AS 'patients_not_18_to_29'
FROM dbo.tests_performed performed
JOIN dbo.test test
ON performed.testID = test.testID
JOIN dbo.visit visit
ON performed.visitID = visit.visitID
JOIN dbo.appointment appt
ON visit.apptID = appt.apptID
JOIN dbo.patient patient
ON appt.patientID = patient.patientID
JOIN dbo.person person
ON patient.personID = person.personID
WHERE performed.datePerformed BETWEEN @startDate AND @endDate 
GROUP BY performed.testID, test.testName
HAVING (COUNT(performed.testID) >= 2)
ORDER BY total_times_performed DESC, performed.testID ASC;
GO