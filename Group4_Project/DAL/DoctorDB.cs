﻿using Group4_Project.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group4_Project.DAL
{
    class DoctorDB
    {
        public static List<Doctor> GetDoctorList()
        {
            List<Doctor> DoctorList = new List<Doctor>();

            string selectStatement = "SELECT p.fname AS FirstName, p.lname AS LastName, " +
                "d.doctorID AS DoctorID, p.personID AS PersonID, ds.specialtyID AS SpecialtyID, " +
                "s.specialtyName AS SpecialtyName " +
                "FROM doctor d " +
                "JOIN person p ON d.personID = p.personID " +
                "JOIN dr_specialties ds ON d.doctorID = ds.doctorID " +
                "JOIN specialty s ON ds.specialtyID = s.specialtyID " +
                "ORDER BY p.lname";


            try
            {
                using (SqlConnection connection = Group4DBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Doctor doctor = new Doctor();
                                doctor.fname = reader["FirstName"].ToString();
                                doctor.lname = reader["LastName"].ToString();
                                doctor.doctorID = (int)reader["DoctorID"];
                                doctor.personID = (int)reader["PersonID"];
                                doctor.drSpecialtyID = (int)reader["SpecialtyID"];
                                doctor.specialtyName = reader["SpecialtyName"].ToString();
                                doctor.displayFullName = doctor.lname + ", " + doctor.fname + " (" + doctor.specialtyName + ") ";

                                DoctorList.Add(doctor);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return DoctorList;
        }
    }
}
