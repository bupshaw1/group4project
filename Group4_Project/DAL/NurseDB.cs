﻿using Group4_Project.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group4_Project.DAL
{
    class NurseDB
    {
        public static Nurse GetNurse(string loginID)
        {
            Nurse nurse = new Nurse();

            string selectStatement =
                "SELECT nurseID AS NurseID, personID AS PersonID, n.userID AS UserID " +
                "FROM nurse n " +
                "JOIN prog_user u ON n.userID = u.userID " +
                "WHERE u.loginID = @loginID";

            try
            {
                using (SqlConnection connection = Group4DBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@loginID", loginID);
                        using (SqlDataReader reader = selectCommand.ExecuteReader(System.Data.CommandBehavior.SingleRow))
                        {
                            if (reader.Read())
                            {
                                nurse.NurseID = (int)reader["NurseID"];
                                nurse.PersonID = (int)reader["PersonID"];
                                nurse.UserID = (int)reader["UserID"];
                            }
                            else
                                nurse = null;
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return nurse;
        }

        public static Nurse GetNurseUser(string login, string pass)
        {
            Nurse user = new Nurse();

            string selectStatement =
                "SELECT nurseID " +
                "FROM dbo.nurse nurse " +
                "JOIN dbo.prog_user prog ON nurse.userID = prog.userID " +
                "WHERE prog.loginID = @login AND prog.password = @pass";

            try
            {
                using (SqlConnection connection = Group4DBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@login", login);
                        selectCommand.Parameters.AddWithValue("@pass", pass);

                        using (SqlDataReader reader = selectCommand.ExecuteReader(System.Data.CommandBehavior.SingleRow))
                        {
                            if (reader.Read())
                            {
                                user.NurseID = (int)reader["nurseID"];
                            }
                            else
                                user = null;
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return user;
        }
    }
}
