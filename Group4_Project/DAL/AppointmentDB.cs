﻿using Group4_Project.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Group4_Project.DAL
{
    class AppointmentDB
    {
        public static List<Appointment> GetAppointments(int patientID)
        {
            List<Appointment> AppointmentList = new List<Appointment>();

            string selectStatement = "SELECT apptID as ApptID, apptDate as ApptDate, apptTime as ApptTime, " +
                "reasonForVisit as ReasonForVisit, a.doctorID as DoctorID, p.fname as DoctorFirstName,  " +
                "p.lname as DoctorLastName " +
                "FROM dbo.appointment a " +
                "JOIN doctor d ON a.doctorID = d.doctorID " +
                "JOIN person p ON d.personID = p.personID " +
                "WHERE patientID = @patientID";

            try
            {
                using (SqlConnection connection = Group4DBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@patientID", patientID);

                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Appointment Appointment = new Appointment();
                                Appointment.ApptID = (int)reader["ApptID"];
                                Appointment.ApptDate = (DateTime)reader["ApptDate"];
                                Appointment.ApptTime = reader["ApptTime"].ToString();
                                Appointment.ReasonForVisit = reader["ReasonForVisit"].ToString();
                                Appointment.DoctorID = (int)reader["DoctorID"];
                                string doctorFirstName = reader["DoctorFirstName"].ToString();
                                string doctorLastName = reader["DoctorLastName"].ToString();
                                Appointment.DoctorFullName = "Dr. " + doctorLastName + ", " + doctorFirstName;
                                Appointment.DisplayFullAppt = ((DateTime)reader["ApptDate"]).ToShortDateString() + " at " + (reader["ApptTime"].ToString()).Substring(0,5);
                                AppointmentList.Add(Appointment);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (InvalidCastException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return AppointmentList;
        }
        public static Appointment GetAppointment(int appointmentID)
        {
            Appointment Appointment = new Appointment();
            SqlConnection connection = Group4DBConnection.GetConnection();
            string selectStatement = "SELECT apptID as ApptID, apptDate as ApptDate, apptTime as ApptTime, " +
                "reasonForVisit as ReasonForVisit, a.doctorID as DoctorID, p.fname as DoctorFirstName,  " +
                "p.lname as DoctorLastName " +
                "FROM dbo.appointment a " +
                "JOIN doctor d ON a.doctorID = d.doctorID " +
                "JOIN person p ON d.personID = p.personID " +
                "WHERE apptID = @apptID";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@apptID", appointmentID);
            SqlDataReader reader = null;
            try
            {
                connection.Open();
                reader = selectCommand.ExecuteReader(CommandBehavior.SingleRow);
                if (reader.Read())
                {
                    Appointment = new Appointment();
                    Appointment.ApptID = (int)reader["ApptID"];
                    Appointment.ApptDate = (DateTime)reader["ApptDate"];
                    Appointment.ApptTime = reader["ApptTime"].ToString();
                    Appointment.ReasonForVisit = reader["ReasonForVisit"].ToString();
                    Appointment.DoctorID = (int)reader["DoctorID"];
                    string doctorFirstName = reader["DoctorFirstName"].ToString();
                    string doctorLastName = reader["DoctorLastName"].ToString();
                    Appointment.DoctorFullName = "Dr. " + doctorLastName + ", " + doctorFirstName;
                    Appointment.DisplayFullAppt = (DateTime)reader["ApptDate"] + " " + reader["ApptTime"].ToString();
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }
            return Appointment;
        }

        public static bool AddAppointment(Appointment appointment)
        {
            bool successful = false;
            SqlConnection connection = Group4DBConnection.GetConnection();
            string insertStatement = "INSERT INTO appointment(ApptDate, ApptTime, ReasonForVisit, PatientID, DoctorID) VALUES (@ApptDate, @ApptTime, @ReasonForVisit, @PatientID, @DoctorID)";
            SqlCommand insertCommand = new SqlCommand(insertStatement, connection);
            insertCommand.Parameters.AddWithValue("@ApptDate", appointment.ApptDate);
            insertCommand.Parameters.AddWithValue("@ApptTime", appointment.ApptTime);
            insertCommand.Parameters.AddWithValue("@ReasonForVisit", appointment.ReasonForVisit);
            insertCommand.Parameters.AddWithValue("@PatientID", appointment.PatientID);
            insertCommand.Parameters.AddWithValue("@DoctorID", appointment.DoctorID);
            try
            {
                connection.Open();
                insertCommand.ExecuteNonQuery();
                successful = true;
            }
            catch (SqlException ex)
            {
                successful = false;
                throw ex;
            }
            catch (Exception ex)
            {
                successful = false;
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
            return successful;
        }

        public static bool UpdateAppointment(Appointment updateAppointment)
        {
            SqlConnection connection = Group4DBConnection.GetConnection();
            string updateStatement = "UPDATE Appointment SET ApptDate = @ApptDate, " +
                "ApptTime = @ApptTime, ReasonForVisit = @Reason, DoctorID = @Doctor " +
                "WHERE apptID = @apptID ";
            SqlCommand updateCommand = new SqlCommand(updateStatement, connection);
            updateCommand.Parameters.AddWithValue("@ApptDate", updateAppointment.ApptDate);
            updateCommand.Parameters.AddWithValue("@ApptTime", updateAppointment.ApptTime);
            updateCommand.Parameters.AddWithValue("@Reason", updateAppointment.ReasonForVisit);
            updateCommand.Parameters.AddWithValue("@Doctor", updateAppointment.DoctorID);
            updateCommand.Parameters.AddWithValue("@apptID", updateAppointment.ApptID);
            SqlDataReader reader = null;
            try
            {
                connection.Open();
                int recCount = updateCommand.ExecuteNonQuery();
                if (recCount > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }
        }


    }
}