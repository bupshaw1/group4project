﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Group4_Project.Model;

namespace Group4_Project.DAL
{
    class VisitDB
    {
        public static Visit GetVisit(int apptID)
        {
            Visit visit = new Visit();

            string selectStatement = "SELECT visitID, bpSystolic, " +
                "bpDiastolic, temperature, pulse, nurseID, symptoms, diagnosis " +
                "FROM visit " +
                "WHERE apptID = @apptID";

            try
            {
                using (SqlConnection connection = Group4DBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@apptID", apptID);

                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                visit.visitID = (int)reader["visitID"];
                                visit.bpSystolic = (int)reader["bpSystolic"];
                                visit.bpDiastolic = (int)reader["bpDiastolic"];
                                visit.temperature = (double)reader["temperature"];
                                visit.pulse = (int)reader["pulse"];
                                visit.nurseID = (int)reader["nurseID"];
                                visit.symptoms = reader["symptoms"].ToString();
                                visit.diagnosis = reader["diagnosis"].ToString();
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return visit;
        }

        public static bool AddVisit(Visit visit)
        {
            bool successful = false;
            SqlConnection connection = Group4DBConnection.GetConnection();
            string insertStatement = "INSERT INTO visit(apptID, bpSystolic, bpDiastolic, temperature, pulse, nurseID, symptoms, diagnosis) " +
                "VALUES (@apptID, @bpSystolic, @bpDiastolic, @temperature, @pulse, @nurseID, @symptoms, @diagnosis)";
            SqlCommand insertCommand = new SqlCommand(insertStatement, connection);
            insertCommand.Parameters.AddWithValue("@apptID", visit.apptID);
            insertCommand.Parameters.AddWithValue("@bpSystolic", visit.bpSystolic);
            insertCommand.Parameters.AddWithValue("@bpDiastolic", visit.bpDiastolic);
            insertCommand.Parameters.AddWithValue("@temperature", visit.temperature);
            insertCommand.Parameters.AddWithValue("@pulse", visit.pulse);
            insertCommand.Parameters.AddWithValue("@nurseID", visit.nurseID);
            insertCommand.Parameters.AddWithValue("@symptoms", visit.symptoms);
            insertCommand.Parameters.AddWithValue("@diagnosis", visit.diagnosis);
            try
            {
                connection.Open();
                insertCommand.ExecuteNonQuery();
                successful = true;
            }
            catch (SqlException ex)
            {
                successful = false;
                throw ex;
            }
            catch (Exception ex)
            {
                successful = false;
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
            return successful;
        }

        public static bool CompleteVisit(Visit visit)
        {
            bool successful = false;
            SqlConnection connection = Group4DBConnection.GetConnection();
            string updateStatement = "UPDATE visit " +
                                        "SET diagnosis = @diagnosis " +
                                        "WHERE visitID = @visitID";
            SqlCommand updateCommand = new SqlCommand(updateStatement, connection);
            updateCommand.Parameters.AddWithValue("@diagnosis", visit.diagnosis);
            updateCommand.Parameters.AddWithValue("@visitID", visit.visitID);
            try
            {
                connection.Open();
                updateCommand.ExecuteNonQuery();
                successful = true;
            }
            catch (SqlException ex)
            {
                successful = false;
                throw ex;
            }
            catch (Exception ex)
            {
                successful = false;
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
            return successful;
        }
    }
}
