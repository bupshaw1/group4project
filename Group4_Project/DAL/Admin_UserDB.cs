﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Group4_Project.Model;

namespace Group4_Project.DAL
{
    public static class Admin_UserDB
    {
        public static Admin_User GetAdminUser(string login, string pass)
        {
            Admin_User user = new Admin_User();

            string selectStatement =
                "SELECT adminID " + 
                "FROM dbo.administrator ad " + 
                "JOIN dbo.prog_user prog ON ad.userID = prog.userID " + 
                "WHERE prog.loginID = @login AND prog.password = @pass";

            try
            {
                using (SqlConnection connection = Group4DBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@login", login);
                        selectCommand.Parameters.AddWithValue("@pass", pass);

                        using (SqlDataReader reader = selectCommand.ExecuteReader(System.Data.CommandBehavior.SingleRow))
                        {
                            if (reader.Read())
                            {
                                user.adminID = reader["adminID"].ToString();
                            }
                            else
                                user = null;
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return user;
        }
    }
}
