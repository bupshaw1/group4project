﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Group4_Project.Model;

namespace Group4_Project.DAL
{
    public static class Prog_UserDB
    {
        public static Prog_User GetProgUser(string login, string pass)
        {
            Prog_User user = new Prog_User();

            string selectStatement =
                "SELECT loginID, password " +
                "FROM prog_user " +
                "WHERE loginID = @login " +
                "AND password = @pass";

            try
            {
                using (SqlConnection connection = Group4DBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@login", login);
                        selectCommand.Parameters.AddWithValue("@pass", pass);

                        using (SqlDataReader reader = selectCommand.ExecuteReader(System.Data.CommandBehavior.SingleRow))
                        {
                            if (reader.Read())
                            {
                                user.LoginID = reader["loginID"].ToString();
                                user.Password = reader["password"].ToString();
                            }
                            else
                                user = null;
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return user;
        }
    }
}
