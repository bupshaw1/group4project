﻿using Group4_Project.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Group4_Project.DAL
{
    class TestDB
    {
        public static List<Test> GetTests(int visitID)
        {
            List<Test> testList = new List<Test>();

            string selectStatement = "SELECT testPerformedID AS TestPerformedID, tp.visitID AS VisitID, " +
                "tp.testID AS TestID, resultsNormal AS ResultsNormal, " +
	            "datePerformed AS DatePerformed, testName AS TestName " +
                "FROM tests_performed tp " +
                "JOIN test t ON tp.testID = t.testID " +
                "JOIN visit v ON tp.visitID = v.visitID " +
                "WHERE tp.visitID = @visitID";

            try
            {
                using (SqlConnection connection = Group4DBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@visitID", visitID);

                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Test test = new Test();
                                test.TestPerformedID = (int)reader["TestPerformedID"];
                                test.VisitID = (int)reader["VisitID"];
                                test.TestID = (int)reader["TestID"];
                                if(reader["ResultsNormal"] != DBNull.Value)
                                {
                                    test.ResultsNormal = (bool)reader["ResultsNormal"];
                                }                               
                                if (reader["DatePerformed"] != DBNull.Value)
                                {
                                    test.DatePerformed = (DateTime)reader["DatePerformed"];
                                }                                  
                                test.TestName = reader["TestName"].ToString();
                                if(reader["ResultsNormal"] != DBNull.Value && reader["DatePerformed"] != DBNull.Value)
                                {
                                    test.TestFullName = test.TestName + " Performed " + test.DatePerformed.ToShortDateString() + " Results Normal: " + test.ResultsNormal;
                                }
                                else
                                {
                                    test.TestFullName = String.Format("{0,-15} {1,15}", test.TestName, "Results not in");
                                }
                                testList.Add(test);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (InvalidCastException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return testList;
        }
        public static Test GetTest(int testPerfomedID)
        {
            Test test;
            SqlConnection connection = Group4DBConnection.GetConnection();
            string selectStatement = "SELECT testPerformedID AS TestPerformedID, tp.visitID AS VisitID, " +
                "tp.testID AS TestID, resultsNormal AS ResultsNormal, " +
                "datePerformed AS DatePerformed, testName AS TestName " +
                "FROM tests_performed tp " +
                "JOIN test t ON tp.testID = t.testID " +
                "JOIN visit v ON tp.visitID = v.visitID " +
                "WHERE tp.testPerformedID = @testPerformedID";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            selectCommand.Parameters.AddWithValue("@testPerformedID", testPerfomedID);
            SqlDataReader reader = null;
            try
            {
                test = new Test();
                connection.Open();
                reader = selectCommand.ExecuteReader(CommandBehavior.SingleRow);
                if (reader.Read())
                {
                    test.TestPerformedID = (int)reader["TestPerformedID"];
                    test.VisitID = (int)reader["VisitID"];
                    test.TestID = (int)reader["TestID"];
                    if (reader["ResultsNormal"] != DBNull.Value)
                    {
                        test.ResultsNormal = (bool)reader["ResultsNormal"];
                    }
                    if (reader["DatePerformed"] != DBNull.Value)
                    {
                        test.DatePerformed = (DateTime)reader["DatePerformed"];
                    }
                    test.TestName = reader["TestName"].ToString();
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }
            return test;
        }

        public static bool OrderTest(Test test)
        {
            bool successful = false;
            SqlConnection connection = Group4DBConnection.GetConnection();
            string insertStatement = "INSERT INTO tests_performed(visitID, testID) VALUES (@VisitID, @TestID)";
            SqlCommand insertCommand = new SqlCommand(insertStatement, connection);
            insertCommand.Parameters.AddWithValue("@VisitID", test.VisitID);
            insertCommand.Parameters.AddWithValue("@TestID", test.TestID);
            try
            {
                connection.Open();
                insertCommand.ExecuteNonQuery();
                successful = true;
            }
            catch (SqlException ex)
            {
                successful = false;
                throw ex;
            }
            catch (Exception ex)
            {
                successful = false;
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }
            return successful;
        }

        public static bool EnterTestResults(Test updateTest)
        {
            SqlConnection connection = Group4DBConnection.GetConnection();
            string updateStatement = "UPDATE tests_performed SET resultsNormal = @ResultsNormal, " +
                "datePerformed = @DatePerformed " +
                "WHERE testPerformedID = @TestPerformedID ";
            SqlCommand updateCommand = new SqlCommand(updateStatement, connection);
            updateCommand.Parameters.AddWithValue("@ResultsNormal", updateTest.ResultsNormal);
            updateCommand.Parameters.AddWithValue("@DatePerformed", updateTest.DatePerformed);
            updateCommand.Parameters.AddWithValue("@TestPerformedID", updateTest.TestPerformedID);
            SqlDataReader reader = null;
            try
            {
                connection.Open();
                int recCount = updateCommand.ExecuteNonQuery();
                if (recCount > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }
        }

        public static List<Test> GetAvailableTests()
        {
            List<Test> AvailableTestList = new List<Test>();

            string selectStatement = "SELECT testID AS TestID, testName AS TestName " +
                "FROM test " +
                "ORDER BY testName";
            try
            {
                using (SqlConnection connection = Group4DBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Test Test = new Test();
                                Test.TestID = (int)reader["TestID"];
                                Test.TestName = reader["TestName"].ToString();
                                AvailableTestList.Add(Test);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (InvalidCastException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return AvailableTestList;
        }
    }
}
