﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Group4_Project.Model;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Group4_Project.DAL
{
    class PatientDB
    {
        public static List<Patient> SearchByName(string lastName, string firstName)
        {
            List<Patient> patientList = new List<Patient>();

            string selectStatement = "SELECT p.fname as FirstName, p.lname as LastName, " +
                "p.dob as DOB, p.address as Address, p.city as City, p.state as State, " +
                "p.zip as Zip, p.phone as Phone, p.gender as Gender, p.ssn as SSN, patient.personID  as PersonID, " +
                "patient.patientID as PatientID " +
                "From dbo.patient patient " +
                "JOIN dbo.person p ON patient.personID = p.personID " +
                "WHERE p.lname = @lastName AND p.fname = @firstName " +
                "ORDER BY LastName, FirstName";

            try
            {
                using (SqlConnection connection = Group4DBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@lastName", lastName);
                        selectCommand.Parameters.AddWithValue("@firstName", firstName);

                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Patient patient = new Model.Patient();
                                patient.fname = reader["FirstName"].ToString();
                                patient.lname = reader["LastName"].ToString();
                                patient.dob = (DateTime)reader["DOB"];
                                patient.address = reader["Address"].ToString();
                                patient.city = reader["City"].ToString();
                                patient.state = reader["State"].ToString();
                                patient.zip = reader["Zip"].ToString();
                                patient.phone = reader["Phone"].ToString();
                                patient.gender = reader["Gender"].ToString();
                                patient.ssn = reader["SSN"].ToString();
                                patient.personID = (int)reader["PersonID"];
                                patient.patientID = (int)reader["PatientID"];
                                patient.displayFullName = patient.lname + ", " + patient.fname + " (" + patient.dob.ToString("d") + ") ";
                                patientList.Add(patient);

                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return patientList;
        }

        public static Patient GetPatient(int patientID)
        {
            Patient patient = new Patient();

            string selectStatement = "SELECT p.fname as FirstName, p.lname as LastName, " +
                "p.dob as DOB, p.address as Address, p.city as City, p.state as State, " +
                "p.zip as Zip, p.phone as Phone, p.gender as Gender, p.ssn as SSN, patient.personID  as PersonID, " +
                "patient.patientID as PatientID " +
                "From dbo.patient patient " +
                "JOIN dbo.person p ON patient.personID = p.personID " +
                "WHERE patient.patientID = @patientID";
            
            try
            {
                using (SqlConnection connection = Group4DBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@patientID", patientID);

                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                patient.fname = reader["FirstName"].ToString();
                                patient.lname = reader["LastName"].ToString();
                                patient.dob = (DateTime)reader["DOB"];
                                patient.address = reader["Address"].ToString();
                                patient.city = reader["City"].ToString();
                                patient.state = reader["State"].ToString();
                                patient.zip = reader["Zip"].ToString();
                                patient.phone = reader["Phone"].ToString();
                                patient.gender = reader["Gender"].ToString();
                                patient.ssn = reader["SSN"].ToString();
                                patient.personID = (int)reader["PersonID"];
                                patient.patientID = (int)reader["PatientID"];
                                patient.displayFullName = patient.lname + ", " + patient.fname + " (" + patient.dob.ToString("d") + ") ";
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return patient;
        }


        public static List<Patient> SearchByLastAndDOB(string lastName, DateTime dob)
        {
            List<Patient> patientList = new List<Patient>();

            string selectStatement = "SELECT p.fname as FirstName, p.lname as LastName, " +
                "p.dob as DOB, p.address as Address, p.city as City, p.state as State, " +
                "p.zip as Zip, p.phone as Phone, p.gender as Gender, p.ssn as SSN, patient.personID  as PersonID, " +
                "patient.patientID as PatientID " +
                "From dbo.patient patient " +
                "JOIN dbo.person p ON patient.personID = p.personID " +
                "WHERE p.lname = @lastName AND p.dob = @dob " +
                "ORDER BY LastName, FirstName";  

            try
            {
                using (SqlConnection connection = Group4DBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@lastName", lastName);
                        selectCommand.Parameters.AddWithValue("@dob", dob);

                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Patient patient = new Model.Patient();
                                patient.fname = reader["FirstName"].ToString();
                                patient.lname = reader["LastName"].ToString();
                                patient.dob = (DateTime)reader["DOB"];
                                patient.address = reader["Address"].ToString();
                                patient.city = reader["City"].ToString();
                                patient.state = reader["State"].ToString();
                                patient.zip = reader["Zip"].ToString();
                                patient.phone = reader["Phone"].ToString();
                                patient.gender = reader["Gender"].ToString();
                                patient.ssn = reader["SSN"].ToString();
                                patient.personID = (int)reader["PersonID"];
                                patient.patientID = (int)reader["PatientID"];
                                patient.displayFullName = patient.lname + ", " + patient.fname + " (" + patient.dob.ToString("d") + ") ";
                                patientList.Add(patient);

                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return patientList;
        }

        public static List<Patient> SearchByDOB(DateTime dob)
        {
            List<Patient> patientList = new List<Patient>();

            string selectStatement = "SELECT p.fname as FirstName, p.lname as LastName, " +
                "p.dob as DOB, p.address as Address, p.city as City, p.state as State, " +
                "p.zip as Zip, p.phone as Phone, p.gender as Gender, p.ssn as SSN, patient.personID  as PersonID, " +
                "patient.patientID as PatientID " +
                "From dbo.patient patient " +
                "JOIN dbo.person p ON patient.personID = p.personID " +
                "WHERE p.dob = @dob " +
                "ORDER BY LastName, FirstName";

            try
            {
                using (SqlConnection connection = Group4DBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@dob", dob);

                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Patient patient = new Model.Patient();
                                patient.fname = reader["FirstName"].ToString();
                                patient.lname = reader["LastName"].ToString();
                                patient.dob = (DateTime)reader["DOB"];
                                patient.address = reader["Address"].ToString();
                                patient.city = reader["City"].ToString();
                                patient.state = reader["State"].ToString();
                                patient.zip = reader["Zip"].ToString();
                                patient.phone = reader["Phone"].ToString();
                                patient.gender = reader["Gender"].ToString();
                                patient.ssn = reader["SSN"].ToString();
                                patient.personID = (int)reader["PersonID"];
                                patient.patientID = (int)reader["PatientID"];
                                patient.displayFullName = patient.lname + ", " + patient.fname + " (" + patient.dob.ToString("d") + ") ";
                                patientList.Add(patient);

                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return patientList;
        }

        public static bool AddPatient(Patient patient)
        {
            SqlConnection connection = Group4DBConnection.GetConnection();
            SqlTransaction addPatientTran = null;

            SqlCommand personCommand = new SqlCommand();
            personCommand.Connection = connection;
            personCommand.CommandText = 
                "INSERT person " +
                " (fname, lname, dob, address, city, state, zip, phone, gender, ssn) " +
                "VALUES (@fname, @lname, @dob, @address, @city, @state, @zip, @phone, @gender, @ssn)";

            personCommand.Parameters.AddWithValue("@fname", patient.fname);
            personCommand.Parameters.AddWithValue("@lname", patient.lname);
            personCommand.Parameters.AddWithValue("@dob", patient.dob);
            personCommand.Parameters.AddWithValue("@address", patient.address);
            personCommand.Parameters.AddWithValue("@city", patient.city);
            personCommand.Parameters.AddWithValue("@state", patient.state);
            personCommand.Parameters.AddWithValue("@zip", patient.zip);
            personCommand.Parameters.AddWithValue("@phone", patient.phone);
            personCommand.Parameters.AddWithValue("@gender", patient.gender);
            personCommand.Parameters.AddWithValue("@ssn", patient.ssn);

            SqlCommand patientCommand = new SqlCommand();
            patientCommand.Connection = connection;
            patientCommand.CommandText = 
                "INSERT patient " +
                " (personID) VALUES (@@identity)";

            try
            {
                connection.Open();
                addPatientTran = connection.BeginTransaction();
                personCommand.Transaction = addPatientTran;
                patientCommand.Transaction = addPatientTran;

                int count = personCommand.ExecuteNonQuery();
                if (count > 0)
                {
                    count = patientCommand.ExecuteNonQuery();
                    if (count > 0)
                    {
                        addPatientTran.Commit();
                        return true;
                    }
                    else
                    {
                        addPatientTran.Rollback();
                        return false;
                    }
                }
                else
                {
                    addPatientTran.Rollback();
                    return false;
                }
            }
            catch (SqlException ex)
            {
                if (addPatientTran != null)
                    addPatientTran.Rollback();
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public static bool UpdatePatientValidate(Patient oldPatient, Patient newPatient)
        {
            SqlConnection connection = Group4DBConnection.GetConnection();
            string updateStatement =
                "UPDATE person SET " +
                "fname = @NewFName, " +
                "lname = @NewLName, " +
                "dob = @NewDOB, " +
                "address = @NewAddress, " +
                "city = @NewCity, " +
                "state = @NewState, " +
                "zip = @NewZip, " +
                "phone = @NewPhone, " +
                "gender = @NewGender, " +
                "ssn = @NewSSN " +
                "WHERE personID = @OldPersonID " +
                "AND fname = @OldFName " +
                "AND lname = @OldLName " +
                "AND dob = @OldDOB " +
                "AND address = @OldAddress " +
                "AND city = @OldCity " +
                "AND state = @OldState " +
                "AND zip = @OldZip " +
                "AND phone = @OldPhone " +
                "AND gender = @OldGender " +
                "AND ssn = @OldSSN";

            SqlCommand updateCommand = new SqlCommand(updateStatement, connection);
            updateCommand.Parameters.AddWithValue("@NewFName", newPatient.fname);
            updateCommand.Parameters.AddWithValue("@NewLName", newPatient.lname);
            updateCommand.Parameters.AddWithValue("@NewDOB", newPatient.dob);
            updateCommand.Parameters.AddWithValue("@NewAddress", newPatient.address);
            updateCommand.Parameters.AddWithValue("@NewCity", newPatient.city);
            updateCommand.Parameters.AddWithValue("@NewState", newPatient.state);
            updateCommand.Parameters.AddWithValue("@NewZip", newPatient.zip);
            updateCommand.Parameters.AddWithValue("@NewPhone", newPatient.phone);
            updateCommand.Parameters.AddWithValue("@NewGender", newPatient.gender);
            updateCommand.Parameters.AddWithValue("@NewSSN", newPatient.ssn);

            updateCommand.Parameters.AddWithValue("@OldFName", oldPatient.fname);
            updateCommand.Parameters.AddWithValue("@OldLName", oldPatient.lname);
            updateCommand.Parameters.AddWithValue("@OldDOB", oldPatient.dob);
            updateCommand.Parameters.AddWithValue("@OldAddress", oldPatient.address);
            updateCommand.Parameters.AddWithValue("@OldCity", oldPatient.city);
            updateCommand.Parameters.AddWithValue("@OldState", oldPatient.state);
            updateCommand.Parameters.AddWithValue("@OldZip", oldPatient.zip);
            updateCommand.Parameters.AddWithValue("@OldPhone", oldPatient.phone);
            updateCommand.Parameters.AddWithValue("@OldGender", oldPatient.gender);
            updateCommand.Parameters.AddWithValue("@OldSSN", oldPatient.ssn);
            updateCommand.Parameters.AddWithValue("@OldPersonID", oldPatient.personID);

            try
            {
                connection.Open();
                int count = updateCommand.ExecuteNonQuery();
                if (count > 0)
                    return true;
                else
                    return false;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public static int UpdatePatient(Patient patient)
        {
            SqlConnection connection = Group4DBConnection.GetConnection();
            string updateStatement =
                "UPDATE person SET " +
                "fname = @FName, " +
                "lname = @LName, " +
                "dob = @DOB, " +
                "address = @Address, " +
                "city = @City, " +
                "state = @State, " +
                "zip = @Zip, " +
                "phone = @Phone, " +
                "gender = @Gender, " +
                "ssn = @SSN " +
                "WHERE personID = @PersonID";

            SqlCommand updateCommand = new SqlCommand(updateStatement, connection);
            updateCommand.Parameters.AddWithValue("@Fname", patient.fname);
            updateCommand.Parameters.AddWithValue("@LName", patient.lname);
            updateCommand.Parameters.AddWithValue("@DOB", patient.dob);
            updateCommand.Parameters.AddWithValue("@Address", patient.address);
            updateCommand.Parameters.AddWithValue("@City", patient.city);
            updateCommand.Parameters.AddWithValue("@State", patient.state);
            updateCommand.Parameters.AddWithValue("@Zip", patient.zip);
            updateCommand.Parameters.AddWithValue("@Phone", patient.phone);
            updateCommand.Parameters.AddWithValue("@Gender", patient.gender);
            updateCommand.Parameters.AddWithValue("@SSN", patient.ssn);
            updateCommand.Parameters.AddWithValue("@PersonID", patient.personID);

            try
            {
                connection.Open();
                updateCommand.ExecuteNonQuery();
                string selectStatement =
                    "SELECT IDENT_CURRENT('person') FROM person";
                SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
                int personID = Convert.ToInt32(selectCommand.ExecuteScalar());
                return personID;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
