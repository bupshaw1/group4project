﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Group4_Project.Model;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Group4_Project.DAL
{
    class Admin_ReportDB
    {
        public static List<Admin_Report> GetMostPerformedTestsDuringDates(DateTime startDate, DateTime endDate)
        {
            List<Admin_Report> reportList = new List<Admin_Report>();

            string selectStatement = "SELECT performed.testID AS test_code, test.testName AS test_name, COUNT(performed.testID) AS total_times_performed, " +
                "(SELECT COUNT(performed.testID) " +
                "FROM dbo.tests_performed performed " +
                "WHERE performed.datePerformed BETWEEN @startDate AND @endDate HAVING (SELECT COUNT(performed.testID)) >= 2) AS all_tests_total, " +
                "CAST(COUNT(performed.testID / performed.testPerformedID) * 10 AS DECIMAL(10,2)) AS percent_of_total, " +
                "SUM(CASE WHEN performed.resultsNormal = 0 THEN 1 ELSE 0 END) AS 'normal_test_results', " +
                "SUM(CASE WHEN performed.resultsNormal = 1 THEN 1 ELSE 0 END) AS 'abnormal_test_results', " +
                "CAST(SUM(case when datediff(DAY, person.dob, performed.datePerformed) / 365 BETWEEN 18 and 29.99 then 1 else 0 end) " + 
                "AS DECIMAL(10, 2)) / COUNT(performed.testID) AS 'patients_between_18_and_29', " +
                "CAST(SUM(case when datediff(DAY, person.dob, performed.datePerformed) / 365 BETWEEN 0 and 17.99 then 1 " + 
                "when datediff(DAY, person.dob, performed.datePerformed) / 365 BETWEEN 30 and 200 then 1 else 0 end) AS DECIMAL(10, 2)) / " + 
                "COUNT(performed.testID) AS 'patients_outside_18_and_29' " +
                "FROM dbo.tests_performed performed " +
                "JOIN dbo.test test " +
                "ON performed.testID = test.testID " +
                "JOIN dbo.visit visit " +
                "ON performed.visitID = visit.visitID " +
                "JOIN dbo.appointment appt " +
                "ON visit.apptID = appt.apptID " +
                "JOIN dbo.patient patient " +
                "ON appt.patientID = patient.patientID " +
                "JOIN dbo.person person " +
                "ON patient.personID = person.personID " +
                "WHERE performed.datePerformed BETWEEN @startDate AND @endDate " +
                "GROUP BY performed.testID, test.testName " +
                "HAVING(COUNT(performed.testID) >= 2) " +
                "ORDER BY total_times_performed DESC, performed.testID ASC";

            try
            {
                using (SqlConnection connection = Group4DBConnection.GetConnection())
                {
                    connection.Open();
                    
                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        selectCommand.Parameters.AddWithValue("@startDate", startDate);
                        selectCommand.Parameters.AddWithValue("@endDate", endDate);
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Admin_Report report = new Model.Admin_Report();
                                report.TestCode = (int)reader["test_code"];
                                report.TestName = reader["test_name"].ToString();
                                report.TotalTimesPerformed = (int)reader["total_times_performed"];
                                report.AllTestsTotal = (int)reader["all_tests_total"];
                                String PercentOfTotal = ((decimal)reader["percent_of_total"]).ToString();
                                report.PercentOfTotal = Math.Round(System.Convert.ToDecimal(PercentOfTotal), 2);
                                report.NormalTestResults = (int)reader["normal_test_results"];
                                report.AbnormalTestResults = (int)reader["abnormal_test_results"];
                                String PatientsBetween18And29 = ((decimal)reader["patients_between_18_and_29"] * 100).ToString();
                                report.PatientsBetween18And29 = Math.Round(System.Convert.ToDecimal(PatientsBetween18And29), 2);
                                String PatientsOutside18And29 = ((decimal)reader["patients_outside_18_and_29"] * 100).ToString();
                                report.PatientsOutside18And29 = Math.Round(System.Convert.ToDecimal(PatientsOutside18And29), 2);
                                reportList.Add(report);
                            }
                            reader.Close();
                        }
                    }
                    connection.Close();
                }
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return reportList;
        }
    }
}
