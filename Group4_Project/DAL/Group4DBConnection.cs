﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace Group4_Project
{
    public static class Group4DBConnection
    {
        public static SqlConnection GetConnection()
        {
            SqlConnection connection;
            try
            {
                string connectionString =
               "Data Source=localhost; Initial Catalog=CS6232-g4;" +
                "Integrated Security=True";
                connection = new SqlConnection(connectionString);
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return connection;
        }
    }
}
