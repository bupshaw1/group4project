﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Group4_Project.Model;

namespace Group4_Project
{
    public static class StateDB
    {
        public static List<State> GetStateList()
        {
            List<State> stateList = new List<State>();

            string selectStatement =
                "SELECT stateName, stateID " +
                "FROM dbo.stateList " +
                "ORDER BY stateID";

            try
            {
                using (SqlConnection connection = Group4DBConnection.GetConnection())
                {
                    connection.Open();

                    using (SqlCommand selectCommand = new SqlCommand(selectStatement, connection))
                    {
                        using (SqlDataReader reader = selectCommand.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                State state = new State();
                                state.StateName = reader["stateName"].ToString();
                                state.StateID = reader["stateID"].ToString();
                                stateList.Add(state);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return stateList;
        }
    }
}
