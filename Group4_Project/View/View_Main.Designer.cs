﻿namespace Group4_Project.View
{
    partial class View_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adminToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loginToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.signInToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.currentUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.signOutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.txtBox_Result_DOB = new System.Windows.Forms.TextBox();
            this.txtBox_Result_Zip = new System.Windows.Forms.TextBox();
            this.txtBox_Search1_LastName = new System.Windows.Forms.TextBox();
            this.txtBox_Result_SSN = new System.Windows.Forms.TextBox();
            this.txtBox_Result_Gender = new System.Windows.Forms.TextBox();
            this.datePicker_Search3_DOB = new System.Windows.Forms.DateTimePicker();
            this.datePicker_Search2_DOB = new System.Windows.Forms.DateTimePicker();
            this.btn_CreateNewAppointment = new System.Windows.Forms.Button();
            this.btn_Clear = new System.Windows.Forms.Button();
            this.btn_Update = new System.Windows.Forms.Button();
            this.comboBox_Result_State = new System.Windows.Forms.ComboBox();
            this.txtBox_Result_City = new System.Windows.Forms.TextBox();
            this.txtBox_Result_Phone = new System.Windows.Forms.TextBox();
            this.txtBox_Result_Address1 = new System.Windows.Forms.TextBox();
            this.txtBox_Result_FirstName = new System.Windows.Forms.TextBox();
            this.txtBox_Result_LastName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBox_PatientSelect = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_NewPatient = new System.Windows.Forms.Button();
            this.btn_Search = new System.Windows.Forms.Button();
            this.txtBox_Search2_LastName = new System.Windows.Forms.TextBox();
            this.txtBox_Search1_FirstName = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtBox_ApptTime = new System.Windows.Forms.TextBox();
            this.btn_UpdateAppointment = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtBox_Appt_Diagnosis = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtBox_Appt_Pulse = new System.Windows.Forms.TextBox();
            this.txtBox_Appt_Temperature = new System.Windows.Forms.TextBox();
            this.txtBox_Appt_Diastolic = new System.Windows.Forms.TextBox();
            this.txtBox_Appt_Systolic = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtBox_Appt_SymptomsAtCheckIn = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtBox_Appt_ReasonForAppointment = new System.Windows.Forms.TextBox();
            this.txtBox_Appt_Doctor = new System.Windows.Forms.TextBox();
            this.comboBox_AppointmentSelect = new System.Windows.Forms.ComboBox();
            this.txtBox_Appt_FirstName = new System.Windows.Forms.TextBox();
            this.txtBox_Appt_LastName = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.txtBox_CheckIn_Time = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtBox_CheckIn_Date = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.btn_CheckIn_Clear = new System.Windows.Forms.Button();
            this.btn_CheckIn_Complete = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.txtBox_CheckIn_Symptoms = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtBox_CheckIn_Pulse = new System.Windows.Forms.TextBox();
            this.txtBox_CheckIn_Temperature = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtBox_CheckIn_Diastolic = new System.Windows.Forms.TextBox();
            this.txtBox_CheckIn_Systolic = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtBox_CheckIn_ReasonForAppointment = new System.Windows.Forms.TextBox();
            this.txtBox_CheckIn_FirstName = new System.Windows.Forms.TextBox();
            this.txtBox_CheckIn_LastName = new System.Windows.Forms.TextBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.label39 = new System.Windows.Forms.Label();
            this.btn_Lab_EnterResult = new System.Windows.Forms.Button();
            this.label37 = new System.Windows.Forms.Label();
            this.lstBox_Lab_TestsOrdered = new System.Windows.Forms.ListBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.txtBox_Lab_ApptDate = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.btn_Order_Test = new System.Windows.Forms.Button();
            this.comboBox_Lab_Test = new System.Windows.Forms.ComboBox();
            this.txtBox_Lab_FirstName = new System.Windows.Forms.TextBox();
            this.txtBox_Lab_LastName = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.btn_CheckOut_Complete = new System.Windows.Forms.Button();
            this.label41 = new System.Windows.Forms.Label();
            this.txtBox_CheckOut_Diagnosis = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.txtBox_CheckOut_Symptoms = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.txtBox_CheckOut_Pulse = new System.Windows.Forms.TextBox();
            this.txtBox_CheckOut_Temperature = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.txtBox_CheckOut_DiastolicBP = new System.Windows.Forms.TextBox();
            this.txtBox_CheckOut_SystolicBP = new System.Windows.Forms.TextBox();
            this.txtBox_CheckOut_ReasonForVisit = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.txtBox_CheckOut_FirstName = new System.Windows.Forms.TextBox();
            this.txtBox_CheckOut_LastName = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.testDBBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.menuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.testDBBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.adminToolStripMenuItem,
            this.loginToolStripMenuItem,
            this.signInToolStripMenuItem,
            this.currentUserToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(622, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // adminToolStripMenuItem
            // 
            this.adminToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reportToolStripMenuItem});
            this.adminToolStripMenuItem.Name = "adminToolStripMenuItem";
            this.adminToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.adminToolStripMenuItem.Text = "Admin";
            this.adminToolStripMenuItem.Visible = false;
            // 
            // reportToolStripMenuItem
            // 
            this.reportToolStripMenuItem.Name = "reportToolStripMenuItem";
            this.reportToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.reportToolStripMenuItem.Text = "Report";
            this.reportToolStripMenuItem.Click += new System.EventHandler(this.reportToolStripMenuItem_Click);
            // 
            // loginToolStripMenuItem
            // 
            this.loginToolStripMenuItem.Name = "loginToolStripMenuItem";
            this.loginToolStripMenuItem.Size = new System.Drawing.Size(12, 20);
            // 
            // signInToolStripMenuItem
            // 
            this.signInToolStripMenuItem.Name = "signInToolStripMenuItem";
            this.signInToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.signInToolStripMenuItem.Text = "Sign In";
            this.signInToolStripMenuItem.Click += new System.EventHandler(this.signInToolStripMenuItem_Click);
            // 
            // currentUserToolStripMenuItem
            // 
            this.currentUserToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.signOutToolStripMenuItem});
            this.currentUserToolStripMenuItem.Name = "currentUserToolStripMenuItem";
            this.currentUserToolStripMenuItem.Size = new System.Drawing.Size(85, 20);
            this.currentUserToolStripMenuItem.Text = "Current User";
            this.currentUserToolStripMenuItem.Visible = false;
            // 
            // signOutToolStripMenuItem
            // 
            this.signOutToolStripMenuItem.Name = "signOutToolStripMenuItem";
            this.signOutToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.signOutToolStripMenuItem.Text = "Sign Out";
            this.signOutToolStripMenuItem.Visible = false;
            this.signOutToolStripMenuItem.Click += new System.EventHandler(this.signOutToolStripMenuItem_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.tabControl1.ItemSize = new System.Drawing.Size(25, 100);
            this.tabControl1.Location = new System.Drawing.Point(9, 25);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(591, 691);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.BackColor = System.Drawing.Color.LightGray;
            this.tabPage1.Controls.Add(this.label35);
            this.tabPage1.Controls.Add(this.label34);
            this.tabPage1.Controls.Add(this.label33);
            this.tabPage1.Controls.Add(this.label32);
            this.tabPage1.Controls.Add(this.label31);
            this.tabPage1.Controls.Add(this.label30);
            this.tabPage1.Controls.Add(this.txtBox_Result_DOB);
            this.tabPage1.Controls.Add(this.txtBox_Result_Zip);
            this.tabPage1.Controls.Add(this.txtBox_Search1_LastName);
            this.tabPage1.Controls.Add(this.txtBox_Result_SSN);
            this.tabPage1.Controls.Add(this.txtBox_Result_Gender);
            this.tabPage1.Controls.Add(this.datePicker_Search3_DOB);
            this.tabPage1.Controls.Add(this.datePicker_Search2_DOB);
            this.tabPage1.Controls.Add(this.btn_CreateNewAppointment);
            this.tabPage1.Controls.Add(this.btn_Clear);
            this.tabPage1.Controls.Add(this.btn_Update);
            this.tabPage1.Controls.Add(this.comboBox_Result_State);
            this.tabPage1.Controls.Add(this.txtBox_Result_City);
            this.tabPage1.Controls.Add(this.txtBox_Result_Phone);
            this.tabPage1.Controls.Add(this.txtBox_Result_Address1);
            this.tabPage1.Controls.Add(this.txtBox_Result_FirstName);
            this.tabPage1.Controls.Add(this.txtBox_Result_LastName);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.comboBox_PatientSelect);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.btn_NewPatient);
            this.tabPage1.Controls.Add(this.btn_Search);
            this.tabPage1.Controls.Add(this.txtBox_Search2_LastName);
            this.tabPage1.Controls.Add(this.txtBox_Search1_FirstName);
            this.tabPage1.Location = new System.Drawing.Point(104, 4);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(483, 683);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Patient Info";
            this.tabPage1.ToolTipText = "Search for patients to view and update personal information";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(136, 619);
            this.label35.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(269, 13);
            this.label35.TabIndex = 33;
            this.label35.Text = "NOTE: Click on Appointments for Existing Appointments";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(266, 144);
            this.label34.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(25, 12);
            this.label34.TabIndex = 32;
            this.label34.Text = "DOB";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(350, 58);
            this.label33.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(25, 12);
            this.label33.TabIndex = 31;
            this.label33.Text = "DOB";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(170, 60);
            this.label32.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(50, 12);
            this.label32.TabIndex = 30;
            this.label32.Text = "Last Name";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(342, 10);
            this.label31.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(51, 12);
            this.label31.TabIndex = 29;
            this.label31.Text = "First Name";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(170, 10);
            this.label30.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(50, 12);
            this.label30.TabIndex = 28;
            this.label30.Text = "Last Name";
            // 
            // txtBox_Result_DOB
            // 
            this.txtBox_Result_DOB.Location = new System.Drawing.Point(280, 342);
            this.txtBox_Result_DOB.Name = "txtBox_Result_DOB";
            this.txtBox_Result_DOB.Size = new System.Drawing.Size(76, 20);
            this.txtBox_Result_DOB.TabIndex = 12;
            this.txtBox_Result_DOB.Text = "DoB";
            this.txtBox_Result_DOB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBox_Result_Zip
            // 
            this.txtBox_Result_Zip.Location = new System.Drawing.Point(298, 384);
            this.txtBox_Result_Zip.Name = "txtBox_Result_Zip";
            this.txtBox_Result_Zip.Size = new System.Drawing.Size(88, 20);
            this.txtBox_Result_Zip.TabIndex = 16;
            this.txtBox_Result_Zip.Text = "Zip";
            this.txtBox_Result_Zip.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBox_Search1_LastName
            // 
            this.txtBox_Search1_LastName.Location = new System.Drawing.Point(156, 22);
            this.txtBox_Search1_LastName.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_Search1_LastName.Name = "txtBox_Search1_LastName";
            this.txtBox_Search1_LastName.Size = new System.Drawing.Size(76, 20);
            this.txtBox_Search1_LastName.TabIndex = 1;
            this.txtBox_Search1_LastName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBox_Result_SSN
            // 
            this.txtBox_Result_SSN.Location = new System.Drawing.Point(280, 473);
            this.txtBox_Result_SSN.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_Result_SSN.Name = "txtBox_Result_SSN";
            this.txtBox_Result_SSN.Size = new System.Drawing.Size(76, 20);
            this.txtBox_Result_SSN.TabIndex = 18;
            this.txtBox_Result_SSN.Text = "SSN";
            this.txtBox_Result_SSN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBox_Result_Gender
            // 
            this.txtBox_Result_Gender.Location = new System.Drawing.Point(158, 342);
            this.txtBox_Result_Gender.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_Result_Gender.Name = "txtBox_Result_Gender";
            this.txtBox_Result_Gender.Size = new System.Drawing.Size(76, 20);
            this.txtBox_Result_Gender.TabIndex = 11;
            this.txtBox_Result_Gender.Text = "Gender";
            this.txtBox_Result_Gender.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // datePicker_Search3_DOB
            // 
            this.datePicker_Search3_DOB.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datePicker_Search3_DOB.Location = new System.Drawing.Point(240, 156);
            this.datePicker_Search3_DOB.Margin = new System.Windows.Forms.Padding(2);
            this.datePicker_Search3_DOB.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.datePicker_Search3_DOB.Name = "datePicker_Search3_DOB";
            this.datePicker_Search3_DOB.Size = new System.Drawing.Size(85, 20);
            this.datePicker_Search3_DOB.TabIndex = 5;
            this.datePicker_Search3_DOB.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            // 
            // datePicker_Search2_DOB
            // 
            this.datePicker_Search2_DOB.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datePicker_Search2_DOB.Location = new System.Drawing.Point(322, 71);
            this.datePicker_Search2_DOB.Margin = new System.Windows.Forms.Padding(2);
            this.datePicker_Search2_DOB.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.datePicker_Search2_DOB.Name = "datePicker_Search2_DOB";
            this.datePicker_Search2_DOB.Size = new System.Drawing.Size(83, 20);
            this.datePicker_Search2_DOB.TabIndex = 4;
            this.datePicker_Search2_DOB.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            // 
            // btn_CreateNewAppointment
            // 
            this.btn_CreateNewAppointment.Location = new System.Drawing.Point(183, 582);
            this.btn_CreateNewAppointment.Margin = new System.Windows.Forms.Padding(2);
            this.btn_CreateNewAppointment.Name = "btn_CreateNewAppointment";
            this.btn_CreateNewAppointment.Size = new System.Drawing.Size(141, 20);
            this.btn_CreateNewAppointment.TabIndex = 21;
            this.btn_CreateNewAppointment.Text = "Create New Appointment";
            this.btn_CreateNewAppointment.UseVisualStyleBackColor = true;
            this.btn_CreateNewAppointment.Click += new System.EventHandler(this.btn_CreateNewAppointment_Click);
            // 
            // btn_Clear
            // 
            this.btn_Clear.Location = new System.Drawing.Point(280, 537);
            this.btn_Clear.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Clear.Name = "btn_Clear";
            this.btn_Clear.Size = new System.Drawing.Size(74, 21);
            this.btn_Clear.TabIndex = 20;
            this.btn_Clear.Text = "Clear";
            this.btn_Clear.UseVisualStyleBackColor = true;
            this.btn_Clear.Click += new System.EventHandler(this.btn_Clear_Click);
            // 
            // btn_Update
            // 
            this.btn_Update.Location = new System.Drawing.Point(156, 537);
            this.btn_Update.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Update.Name = "btn_Update";
            this.btn_Update.Size = new System.Drawing.Size(74, 20);
            this.btn_Update.TabIndex = 19;
            this.btn_Update.Text = "Update";
            this.btn_Update.UseVisualStyleBackColor = true;
            this.btn_Update.Click += new System.EventHandler(this.btn_Update_Click);
            // 
            // comboBox_Result_State
            // 
            this.comboBox_Result_State.FormattingEnabled = true;
            this.comboBox_Result_State.Location = new System.Drawing.Point(280, 427);
            this.comboBox_Result_State.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox_Result_State.Name = "comboBox_Result_State";
            this.comboBox_Result_State.Size = new System.Drawing.Size(76, 21);
            this.comboBox_Result_State.TabIndex = 15;
            this.comboBox_Result_State.Text = "State";
            // 
            // txtBox_Result_City
            // 
            this.txtBox_Result_City.Location = new System.Drawing.Point(158, 427);
            this.txtBox_Result_City.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_Result_City.Name = "txtBox_Result_City";
            this.txtBox_Result_City.Size = new System.Drawing.Size(76, 20);
            this.txtBox_Result_City.TabIndex = 14;
            this.txtBox_Result_City.Text = "City";
            this.txtBox_Result_City.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBox_Result_Phone
            // 
            this.txtBox_Result_Phone.Location = new System.Drawing.Point(156, 473);
            this.txtBox_Result_Phone.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_Result_Phone.Name = "txtBox_Result_Phone";
            this.txtBox_Result_Phone.Size = new System.Drawing.Size(78, 20);
            this.txtBox_Result_Phone.TabIndex = 17;
            this.txtBox_Result_Phone.Text = "Phone Number";
            this.txtBox_Result_Phone.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBox_Result_Address1
            // 
            this.txtBox_Result_Address1.Location = new System.Drawing.Point(158, 384);
            this.txtBox_Result_Address1.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_Result_Address1.Name = "txtBox_Result_Address1";
            this.txtBox_Result_Address1.Size = new System.Drawing.Size(124, 20);
            this.txtBox_Result_Address1.TabIndex = 13;
            this.txtBox_Result_Address1.Text = "Address1";
            this.txtBox_Result_Address1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBox_Result_FirstName
            // 
            this.txtBox_Result_FirstName.Location = new System.Drawing.Point(280, 296);
            this.txtBox_Result_FirstName.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_Result_FirstName.Name = "txtBox_Result_FirstName";
            this.txtBox_Result_FirstName.Size = new System.Drawing.Size(76, 20);
            this.txtBox_Result_FirstName.TabIndex = 10;
            this.txtBox_Result_FirstName.Text = "First Name";
            this.txtBox_Result_FirstName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBox_Result_LastName
            // 
            this.txtBox_Result_LastName.Location = new System.Drawing.Point(158, 296);
            this.txtBox_Result_LastName.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_Result_LastName.Name = "txtBox_Result_LastName";
            this.txtBox_Result_LastName.Size = new System.Drawing.Size(76, 20);
            this.txtBox_Result_LastName.TabIndex = 9;
            this.txtBox_Result_LastName.Text = "Last Name";
            this.txtBox_Result_LastName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(31, 264);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(98, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Patient Information:";
            // 
            // comboBox_PatientSelect
            // 
            this.comboBox_PatientSelect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_PatientSelect.FormattingEnabled = true;
            this.comboBox_PatientSelect.Location = new System.Drawing.Point(158, 210);
            this.comboBox_PatientSelect.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox_PatientSelect.Name = "comboBox_PatientSelect";
            this.comboBox_PatientSelect.Size = new System.Drawing.Size(228, 21);
            this.comboBox_PatientSelect.TabIndex = 8;
            this.comboBox_PatientSelect.SelectedIndexChanged += new System.EventHandler(this.comboBox_PatientSelect_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(31, 210);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Search Results:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(272, 48);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(16, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "or";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(272, 107);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(16, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "or";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(264, 79);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "and";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 19);
            this.label3.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(264, 24);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "and";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Search Patients:";
            // 
            // btn_NewPatient
            // 
            this.btn_NewPatient.Location = new System.Drawing.Point(31, 132);
            this.btn_NewPatient.Margin = new System.Windows.Forms.Padding(2);
            this.btn_NewPatient.Name = "btn_NewPatient";
            this.btn_NewPatient.Size = new System.Drawing.Size(74, 19);
            this.btn_NewPatient.TabIndex = 7;
            this.btn_NewPatient.Text = "New Patient";
            this.btn_NewPatient.UseVisualStyleBackColor = true;
            this.btn_NewPatient.Click += new System.EventHandler(this.btn_NewPatient_Click);
            // 
            // btn_Search
            // 
            this.btn_Search.Location = new System.Drawing.Point(31, 72);
            this.btn_Search.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.Size = new System.Drawing.Size(74, 19);
            this.btn_Search.TabIndex = 6;
            this.btn_Search.Text = "Search";
            this.btn_Search.UseVisualStyleBackColor = true;
            this.btn_Search.Click += new System.EventHandler(this.btn_Search_Click);
            // 
            // txtBox_Search2_LastName
            // 
            this.txtBox_Search2_LastName.Location = new System.Drawing.Point(156, 72);
            this.txtBox_Search2_LastName.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_Search2_LastName.Name = "txtBox_Search2_LastName";
            this.txtBox_Search2_LastName.Size = new System.Drawing.Size(76, 20);
            this.txtBox_Search2_LastName.TabIndex = 3;
            this.txtBox_Search2_LastName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBox_Search1_FirstName
            // 
            this.txtBox_Search1_FirstName.Location = new System.Drawing.Point(322, 22);
            this.txtBox_Search1_FirstName.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_Search1_FirstName.Name = "txtBox_Search1_FirstName";
            this.txtBox_Search1_FirstName.Size = new System.Drawing.Size(76, 20);
            this.txtBox_Search1_FirstName.TabIndex = 2;
            this.txtBox_Search1_FirstName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.LightGray;
            this.tabPage2.Controls.Add(this.txtBox_ApptTime);
            this.tabPage2.Controls.Add(this.btn_UpdateAppointment);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.label36);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.txtBox_Appt_Diagnosis);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.txtBox_Appt_Pulse);
            this.tabPage2.Controls.Add(this.txtBox_Appt_Temperature);
            this.tabPage2.Controls.Add(this.txtBox_Appt_Diastolic);
            this.tabPage2.Controls.Add(this.txtBox_Appt_Systolic);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.txtBox_Appt_SymptomsAtCheckIn);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.txtBox_Appt_ReasonForAppointment);
            this.tabPage2.Controls.Add(this.txtBox_Appt_Doctor);
            this.tabPage2.Controls.Add(this.comboBox_AppointmentSelect);
            this.tabPage2.Controls.Add(this.txtBox_Appt_FirstName);
            this.tabPage2.Controls.Add(this.txtBox_Appt_LastName);
            this.tabPage2.Location = new System.Drawing.Point(104, 4);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(483, 683);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Appointments";
            this.tabPage2.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // txtBox_ApptTime
            // 
            this.txtBox_ApptTime.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBox_ApptTime.Enabled = false;
            this.txtBox_ApptTime.Location = new System.Drawing.Point(241, 74);
            this.txtBox_ApptTime.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_ApptTime.Name = "txtBox_ApptTime";
            this.txtBox_ApptTime.Size = new System.Drawing.Size(108, 20);
            this.txtBox_ApptTime.TabIndex = 25;
            // 
            // btn_UpdateAppointment
            // 
            this.btn_UpdateAppointment.Location = new System.Drawing.Point(146, 243);
            this.btn_UpdateAppointment.Margin = new System.Windows.Forms.Padding(2);
            this.btn_UpdateAppointment.Name = "btn_UpdateAppointment";
            this.btn_UpdateAppointment.Size = new System.Drawing.Size(119, 26);
            this.btn_UpdateAppointment.TabIndex = 24;
            this.btn_UpdateAppointment.Text = "Update Appointment";
            this.btn_UpdateAppointment.UseVisualStyleBackColor = true;
            this.btn_UpdateAppointment.Click += new System.EventHandler(this.btn_UpdateAppointment_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 266);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(499, 13);
            this.label9.TabIndex = 23;
            this.label9.Text = "_________________________________________________________________________________" +
    "_";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(73, 52);
            this.label36.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(142, 13);
            this.label36.TabIndex = 20;
            this.label36.Text = "Select From Drop Down List:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(65, 481);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 13);
            this.label16.TabIndex = 18;
            this.label16.Text = "Diagnosis";
            // 
            // txtBox_Appt_Diagnosis
            // 
            this.txtBox_Appt_Diagnosis.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBox_Appt_Diagnosis.Enabled = false;
            this.txtBox_Appt_Diagnosis.Location = new System.Drawing.Point(68, 502);
            this.txtBox_Appt_Diagnosis.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_Appt_Diagnosis.Multiline = true;
            this.txtBox_Appt_Diagnosis.Name = "txtBox_Appt_Diagnosis";
            this.txtBox_Appt_Diagnosis.ReadOnly = true;
            this.txtBox_Appt_Diagnosis.Size = new System.Drawing.Size(281, 116);
            this.txtBox_Appt_Diagnosis.TabIndex = 17;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(304, 431);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(33, 13);
            this.label15.TabIndex = 16;
            this.label15.Text = "Pulse";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(213, 431);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 13);
            this.label14.TabIndex = 15;
            this.label14.Text = "Temperature";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(144, 431);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(47, 13);
            this.label13.TabIndex = 14;
            this.label13.Text = "Diastolic";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(65, 431);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(43, 13);
            this.label12.TabIndex = 13;
            this.label12.Text = "Systolic";
            // 
            // txtBox_Appt_Pulse
            // 
            this.txtBox_Appt_Pulse.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBox_Appt_Pulse.Enabled = false;
            this.txtBox_Appt_Pulse.Location = new System.Drawing.Point(307, 447);
            this.txtBox_Appt_Pulse.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_Appt_Pulse.Name = "txtBox_Appt_Pulse";
            this.txtBox_Appt_Pulse.ReadOnly = true;
            this.txtBox_Appt_Pulse.Size = new System.Drawing.Size(42, 20);
            this.txtBox_Appt_Pulse.TabIndex = 12;
            // 
            // txtBox_Appt_Temperature
            // 
            this.txtBox_Appt_Temperature.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBox_Appt_Temperature.Enabled = false;
            this.txtBox_Appt_Temperature.Location = new System.Drawing.Point(226, 447);
            this.txtBox_Appt_Temperature.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_Appt_Temperature.Name = "txtBox_Appt_Temperature";
            this.txtBox_Appt_Temperature.ReadOnly = true;
            this.txtBox_Appt_Temperature.Size = new System.Drawing.Size(42, 20);
            this.txtBox_Appt_Temperature.TabIndex = 11;
            // 
            // txtBox_Appt_Diastolic
            // 
            this.txtBox_Appt_Diastolic.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBox_Appt_Diastolic.Enabled = false;
            this.txtBox_Appt_Diastolic.Location = new System.Drawing.Point(146, 447);
            this.txtBox_Appt_Diastolic.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_Appt_Diastolic.Name = "txtBox_Appt_Diastolic";
            this.txtBox_Appt_Diastolic.ReadOnly = true;
            this.txtBox_Appt_Diastolic.Size = new System.Drawing.Size(42, 20);
            this.txtBox_Appt_Diastolic.TabIndex = 10;
            // 
            // txtBox_Appt_Systolic
            // 
            this.txtBox_Appt_Systolic.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBox_Appt_Systolic.Enabled = false;
            this.txtBox_Appt_Systolic.Location = new System.Drawing.Point(68, 447);
            this.txtBox_Appt_Systolic.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_Appt_Systolic.Name = "txtBox_Appt_Systolic";
            this.txtBox_Appt_Systolic.ReadOnly = true;
            this.txtBox_Appt_Systolic.Size = new System.Drawing.Size(42, 20);
            this.txtBox_Appt_Systolic.TabIndex = 9;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(65, 284);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(113, 13);
            this.label11.TabIndex = 8;
            this.label11.Text = "Symptoms at Check-In";
            // 
            // txtBox_Appt_SymptomsAtCheckIn
            // 
            this.txtBox_Appt_SymptomsAtCheckIn.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBox_Appt_SymptomsAtCheckIn.Enabled = false;
            this.txtBox_Appt_SymptomsAtCheckIn.Location = new System.Drawing.Point(68, 301);
            this.txtBox_Appt_SymptomsAtCheckIn.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_Appt_SymptomsAtCheckIn.Multiline = true;
            this.txtBox_Appt_SymptomsAtCheckIn.Name = "txtBox_Appt_SymptomsAtCheckIn";
            this.txtBox_Appt_SymptomsAtCheckIn.ReadOnly = true;
            this.txtBox_Appt_SymptomsAtCheckIn.Size = new System.Drawing.Size(281, 116);
            this.txtBox_Appt_SymptomsAtCheckIn.TabIndex = 7;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(65, 108);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(121, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "Reason for Appointment";
            // 
            // txtBox_Appt_ReasonForAppointment
            // 
            this.txtBox_Appt_ReasonForAppointment.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBox_Appt_ReasonForAppointment.Enabled = false;
            this.txtBox_Appt_ReasonForAppointment.Location = new System.Drawing.Point(68, 124);
            this.txtBox_Appt_ReasonForAppointment.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_Appt_ReasonForAppointment.Multiline = true;
            this.txtBox_Appt_ReasonForAppointment.Name = "txtBox_Appt_ReasonForAppointment";
            this.txtBox_Appt_ReasonForAppointment.Size = new System.Drawing.Size(281, 116);
            this.txtBox_Appt_ReasonForAppointment.TabIndex = 5;
            // 
            // txtBox_Appt_Doctor
            // 
            this.txtBox_Appt_Doctor.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBox_Appt_Doctor.Enabled = false;
            this.txtBox_Appt_Doctor.Location = new System.Drawing.Point(241, 103);
            this.txtBox_Appt_Doctor.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_Appt_Doctor.Name = "txtBox_Appt_Doctor";
            this.txtBox_Appt_Doctor.Size = new System.Drawing.Size(108, 20);
            this.txtBox_Appt_Doctor.TabIndex = 4;
            // 
            // comboBox_AppointmentSelect
            // 
            this.comboBox_AppointmentSelect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_AppointmentSelect.FormattingEnabled = true;
            this.comboBox_AppointmentSelect.Location = new System.Drawing.Point(68, 72);
            this.comboBox_AppointmentSelect.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox_AppointmentSelect.Name = "comboBox_AppointmentSelect";
            this.comboBox_AppointmentSelect.Size = new System.Drawing.Size(169, 21);
            this.comboBox_AppointmentSelect.TabIndex = 3;
            this.comboBox_AppointmentSelect.SelectedIndexChanged += new System.EventHandler(this.comboBox_AppointmentSelect_SelectedIndexChanged);
            // 
            // txtBox_Appt_FirstName
            // 
            this.txtBox_Appt_FirstName.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBox_Appt_FirstName.Enabled = false;
            this.txtBox_Appt_FirstName.Location = new System.Drawing.Point(215, 20);
            this.txtBox_Appt_FirstName.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_Appt_FirstName.Name = "txtBox_Appt_FirstName";
            this.txtBox_Appt_FirstName.Size = new System.Drawing.Size(101, 20);
            this.txtBox_Appt_FirstName.TabIndex = 2;
            // 
            // txtBox_Appt_LastName
            // 
            this.txtBox_Appt_LastName.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBox_Appt_LastName.Enabled = false;
            this.txtBox_Appt_LastName.Location = new System.Drawing.Point(102, 20);
            this.txtBox_Appt_LastName.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_Appt_LastName.Name = "txtBox_Appt_LastName";
            this.txtBox_Appt_LastName.Size = new System.Drawing.Size(110, 20);
            this.txtBox_Appt_LastName.TabIndex = 1;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.LightGray;
            this.tabPage4.Controls.Add(this.txtBox_CheckIn_Time);
            this.tabPage4.Controls.Add(this.label22);
            this.tabPage4.Controls.Add(this.txtBox_CheckIn_Date);
            this.tabPage4.Controls.Add(this.label21);
            this.tabPage4.Controls.Add(this.label20);
            this.tabPage4.Controls.Add(this.label19);
            this.tabPage4.Controls.Add(this.btn_CheckIn_Clear);
            this.tabPage4.Controls.Add(this.btn_CheckIn_Complete);
            this.tabPage4.Controls.Add(this.label29);
            this.tabPage4.Controls.Add(this.txtBox_CheckIn_Symptoms);
            this.tabPage4.Controls.Add(this.label28);
            this.tabPage4.Controls.Add(this.label27);
            this.tabPage4.Controls.Add(this.txtBox_CheckIn_Pulse);
            this.tabPage4.Controls.Add(this.txtBox_CheckIn_Temperature);
            this.tabPage4.Controls.Add(this.label26);
            this.tabPage4.Controls.Add(this.label25);
            this.tabPage4.Controls.Add(this.txtBox_CheckIn_Diastolic);
            this.tabPage4.Controls.Add(this.txtBox_CheckIn_Systolic);
            this.tabPage4.Controls.Add(this.label24);
            this.tabPage4.Controls.Add(this.txtBox_CheckIn_ReasonForAppointment);
            this.tabPage4.Controls.Add(this.txtBox_CheckIn_FirstName);
            this.tabPage4.Controls.Add(this.txtBox_CheckIn_LastName);
            this.tabPage4.Location = new System.Drawing.Point(104, 4);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(483, 683);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Check-In";
            // 
            // txtBox_CheckIn_Time
            // 
            this.txtBox_CheckIn_Time.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBox_CheckIn_Time.Enabled = false;
            this.txtBox_CheckIn_Time.Location = new System.Drawing.Point(276, 67);
            this.txtBox_CheckIn_Time.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_CheckIn_Time.Name = "txtBox_CheckIn_Time";
            this.txtBox_CheckIn_Time.Size = new System.Drawing.Size(105, 20);
            this.txtBox_CheckIn_Time.TabIndex = 23;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(236, 69);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(33, 13);
            this.label22.TabIndex = 22;
            this.label22.Text = "Time:";
            // 
            // txtBox_CheckIn_Date
            // 
            this.txtBox_CheckIn_Date.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBox_CheckIn_Date.Enabled = false;
            this.txtBox_CheckIn_Date.Location = new System.Drawing.Point(102, 67);
            this.txtBox_CheckIn_Date.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_CheckIn_Date.Name = "txtBox_CheckIn_Date";
            this.txtBox_CheckIn_Date.Size = new System.Drawing.Size(132, 20);
            this.txtBox_CheckIn_Date.TabIndex = 21;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(62, 69);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(33, 13);
            this.label21.TabIndex = 20;
            this.label21.Text = "Date:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(62, 40);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(43, 13);
            this.label20.TabIndex = 19;
            this.label20.Text = "Patient:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(123, 274);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(12, 17);
            this.label19.TabIndex = 18;
            this.label19.Text = "/";
            // 
            // btn_CheckIn_Clear
            // 
            this.btn_CheckIn_Clear.Location = new System.Drawing.Point(262, 500);
            this.btn_CheckIn_Clear.Margin = new System.Windows.Forms.Padding(2);
            this.btn_CheckIn_Clear.Name = "btn_CheckIn_Clear";
            this.btn_CheckIn_Clear.Size = new System.Drawing.Size(70, 22);
            this.btn_CheckIn_Clear.TabIndex = 17;
            this.btn_CheckIn_Clear.Text = "Clear";
            this.btn_CheckIn_Clear.UseVisualStyleBackColor = true;
            this.btn_CheckIn_Clear.Click += new System.EventHandler(this.btn_CheckIn_Clear_Click);
            // 
            // btn_CheckIn_Complete
            // 
            this.btn_CheckIn_Complete.Location = new System.Drawing.Point(107, 500);
            this.btn_CheckIn_Complete.Margin = new System.Windows.Forms.Padding(2);
            this.btn_CheckIn_Complete.Name = "btn_CheckIn_Complete";
            this.btn_CheckIn_Complete.Size = new System.Drawing.Size(70, 22);
            this.btn_CheckIn_Complete.TabIndex = 16;
            this.btn_CheckIn_Complete.Text = "Complete";
            this.btn_CheckIn_Complete.UseVisualStyleBackColor = true;
            this.btn_CheckIn_Complete.Click += new System.EventHandler(this.btn_CheckIn_Complete_Click);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(62, 301);
            this.label29.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(55, 13);
            this.label29.TabIndex = 15;
            this.label29.Text = "Symptoms";
            // 
            // txtBox_CheckIn_Symptoms
            // 
            this.txtBox_CheckIn_Symptoms.Location = new System.Drawing.Point(62, 320);
            this.txtBox_CheckIn_Symptoms.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_CheckIn_Symptoms.Multiline = true;
            this.txtBox_CheckIn_Symptoms.Name = "txtBox_CheckIn_Symptoms";
            this.txtBox_CheckIn_Symptoms.Size = new System.Drawing.Size(320, 155);
            this.txtBox_CheckIn_Symptoms.TabIndex = 14;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(300, 258);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(33, 13);
            this.label28.TabIndex = 13;
            this.label28.Text = "Pulse";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(214, 258);
            this.label27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(67, 13);
            this.label27.TabIndex = 12;
            this.label27.Text = "Temperature";
            // 
            // txtBox_CheckIn_Pulse
            // 
            this.txtBox_CheckIn_Pulse.Location = new System.Drawing.Point(292, 274);
            this.txtBox_CheckIn_Pulse.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_CheckIn_Pulse.Name = "txtBox_CheckIn_Pulse";
            this.txtBox_CheckIn_Pulse.Size = new System.Drawing.Size(53, 20);
            this.txtBox_CheckIn_Pulse.TabIndex = 11;
            // 
            // txtBox_CheckIn_Temperature
            // 
            this.txtBox_CheckIn_Temperature.Location = new System.Drawing.Point(223, 274);
            this.txtBox_CheckIn_Temperature.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_CheckIn_Temperature.Name = "txtBox_CheckIn_Temperature";
            this.txtBox_CheckIn_Temperature.Size = new System.Drawing.Size(53, 20);
            this.txtBox_CheckIn_Temperature.TabIndex = 10;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(140, 258);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(64, 13);
            this.label26.TabIndex = 9;
            this.label26.Text = "Diastolic BP";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(60, 258);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(60, 13);
            this.label25.TabIndex = 8;
            this.label25.Text = "Systolic BP";
            // 
            // txtBox_CheckIn_Diastolic
            // 
            this.txtBox_CheckIn_Diastolic.Location = new System.Drawing.Point(142, 274);
            this.txtBox_CheckIn_Diastolic.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_CheckIn_Diastolic.Name = "txtBox_CheckIn_Diastolic";
            this.txtBox_CheckIn_Diastolic.Size = new System.Drawing.Size(53, 20);
            this.txtBox_CheckIn_Diastolic.TabIndex = 7;
            // 
            // txtBox_CheckIn_Systolic
            // 
            this.txtBox_CheckIn_Systolic.Location = new System.Drawing.Point(62, 274);
            this.txtBox_CheckIn_Systolic.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_CheckIn_Systolic.Name = "txtBox_CheckIn_Systolic";
            this.txtBox_CheckIn_Systolic.Size = new System.Drawing.Size(53, 20);
            this.txtBox_CheckIn_Systolic.TabIndex = 6;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(60, 109);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(81, 13);
            this.label24.TabIndex = 4;
            this.label24.Text = "Reason for Visit";
            // 
            // txtBox_CheckIn_ReasonForAppointment
            // 
            this.txtBox_CheckIn_ReasonForAppointment.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBox_CheckIn_ReasonForAppointment.Enabled = false;
            this.txtBox_CheckIn_ReasonForAppointment.Location = new System.Drawing.Point(60, 128);
            this.txtBox_CheckIn_ReasonForAppointment.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_CheckIn_ReasonForAppointment.Multiline = true;
            this.txtBox_CheckIn_ReasonForAppointment.Name = "txtBox_CheckIn_ReasonForAppointment";
            this.txtBox_CheckIn_ReasonForAppointment.Size = new System.Drawing.Size(320, 112);
            this.txtBox_CheckIn_ReasonForAppointment.TabIndex = 3;
            // 
            // txtBox_CheckIn_FirstName
            // 
            this.txtBox_CheckIn_FirstName.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBox_CheckIn_FirstName.Enabled = false;
            this.txtBox_CheckIn_FirstName.Location = new System.Drawing.Point(237, 40);
            this.txtBox_CheckIn_FirstName.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_CheckIn_FirstName.Name = "txtBox_CheckIn_FirstName";
            this.txtBox_CheckIn_FirstName.Size = new System.Drawing.Size(143, 20);
            this.txtBox_CheckIn_FirstName.TabIndex = 2;
            // 
            // txtBox_CheckIn_LastName
            // 
            this.txtBox_CheckIn_LastName.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBox_CheckIn_LastName.Enabled = false;
            this.txtBox_CheckIn_LastName.Location = new System.Drawing.Point(102, 40);
            this.txtBox_CheckIn_LastName.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_CheckIn_LastName.Name = "txtBox_CheckIn_LastName";
            this.txtBox_CheckIn_LastName.Size = new System.Drawing.Size(132, 20);
            this.txtBox_CheckIn_LastName.TabIndex = 1;
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.Color.LightGray;
            this.tabPage5.Controls.Add(this.label39);
            this.tabPage5.Controls.Add(this.btn_Lab_EnterResult);
            this.tabPage5.Controls.Add(this.label37);
            this.tabPage5.Controls.Add(this.lstBox_Lab_TestsOrdered);
            this.tabPage5.Controls.Add(this.label23);
            this.tabPage5.Controls.Add(this.label38);
            this.tabPage5.Controls.Add(this.txtBox_Lab_ApptDate);
            this.tabPage5.Controls.Add(this.label18);
            this.tabPage5.Controls.Add(this.btn_Order_Test);
            this.tabPage5.Controls.Add(this.comboBox_Lab_Test);
            this.tabPage5.Controls.Add(this.txtBox_Lab_FirstName);
            this.tabPage5.Controls.Add(this.txtBox_Lab_LastName);
            this.tabPage5.Controls.Add(this.label17);
            this.tabPage5.Location = new System.Drawing.Point(104, 4);
            this.tabPage5.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(483, 683);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Lab Tests";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(135, 456);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(245, 13);
            this.label39.TabIndex = 34;
            this.label39.Text = "Choose a test from above and click \"Enter Result\"";
            // 
            // btn_Lab_EnterResult
            // 
            this.btn_Lab_EnterResult.Location = new System.Drawing.Point(234, 472);
            this.btn_Lab_EnterResult.Name = "btn_Lab_EnterResult";
            this.btn_Lab_EnterResult.Size = new System.Drawing.Size(75, 23);
            this.btn_Lab_EnterResult.TabIndex = 33;
            this.btn_Lab_EnterResult.Text = "Enter Result";
            this.btn_Lab_EnterResult.UseVisualStyleBackColor = true;
            this.btn_Lab_EnterResult.Click += new System.EventHandler(this.btn_Lab_EnterResult_Click);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(94, 238);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(77, 13);
            this.label37.TabIndex = 32;
            this.label37.Text = "Ordered Tests:";
            // 
            // lstBox_Lab_TestsOrdered
            // 
            this.lstBox_Lab_TestsOrdered.FormattingEnabled = true;
            this.lstBox_Lab_TestsOrdered.Location = new System.Drawing.Point(98, 262);
            this.lstBox_Lab_TestsOrdered.Name = "lstBox_Lab_TestsOrdered";
            this.lstBox_Lab_TestsOrdered.Size = new System.Drawing.Size(355, 186);
            this.lstBox_Lab_TestsOrdered.TabIndex = 31;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(44, 107);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(69, 13);
            this.label23.TabIndex = 29;
            this.label23.Text = "Appointment:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(185, 17);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(100, 16);
            this.label38.TabIndex = 28;
            this.label38.Text = "Order Test(s)";
            // 
            // txtBox_Lab_ApptDate
            // 
            this.txtBox_Lab_ApptDate.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBox_Lab_ApptDate.Enabled = false;
            this.txtBox_Lab_ApptDate.Location = new System.Drawing.Point(118, 104);
            this.txtBox_Lab_ApptDate.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_Lab_ApptDate.Name = "txtBox_Lab_ApptDate";
            this.txtBox_Lab_ApptDate.Size = new System.Drawing.Size(274, 20);
            this.txtBox_Lab_ApptDate.TabIndex = 25;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(118, 163);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(70, 13);
            this.label18.TabIndex = 13;
            this.label18.Text = "Choose Test:";
            // 
            // btn_Order_Test
            // 
            this.btn_Order_Test.Location = new System.Drawing.Point(367, 182);
            this.btn_Order_Test.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Order_Test.Name = "btn_Order_Test";
            this.btn_Order_Test.Size = new System.Drawing.Size(77, 19);
            this.btn_Order_Test.TabIndex = 12;
            this.btn_Order_Test.Text = "Order Test";
            this.btn_Order_Test.UseVisualStyleBackColor = true;
            this.btn_Order_Test.Click += new System.EventHandler(this.btn_Order_Test_Click);
            // 
            // comboBox_Lab_Test
            // 
            this.comboBox_Lab_Test.FormattingEnabled = true;
            this.comboBox_Lab_Test.Location = new System.Drawing.Point(118, 182);
            this.comboBox_Lab_Test.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox_Lab_Test.Name = "comboBox_Lab_Test";
            this.comboBox_Lab_Test.Size = new System.Drawing.Size(201, 21);
            this.comboBox_Lab_Test.TabIndex = 11;
            // 
            // txtBox_Lab_FirstName
            // 
            this.txtBox_Lab_FirstName.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBox_Lab_FirstName.Enabled = false;
            this.txtBox_Lab_FirstName.Location = new System.Drawing.Point(259, 69);
            this.txtBox_Lab_FirstName.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_Lab_FirstName.Name = "txtBox_Lab_FirstName";
            this.txtBox_Lab_FirstName.Size = new System.Drawing.Size(133, 20);
            this.txtBox_Lab_FirstName.TabIndex = 10;
            // 
            // txtBox_Lab_LastName
            // 
            this.txtBox_Lab_LastName.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBox_Lab_LastName.Enabled = false;
            this.txtBox_Lab_LastName.Location = new System.Drawing.Point(118, 69);
            this.txtBox_Lab_LastName.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_Lab_LastName.Name = "txtBox_Lab_LastName";
            this.txtBox_Lab_LastName.Size = new System.Drawing.Size(137, 20);
            this.txtBox_Lab_LastName.TabIndex = 9;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(70, 72);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(43, 13);
            this.label17.TabIndex = 8;
            this.label17.Text = "Patient:";
            // 
            // tabPage7
            // 
            this.tabPage7.BackColor = System.Drawing.Color.LightGray;
            this.tabPage7.Controls.Add(this.btn_CheckOut_Complete);
            this.tabPage7.Controls.Add(this.label41);
            this.tabPage7.Controls.Add(this.txtBox_CheckOut_Diagnosis);
            this.tabPage7.Controls.Add(this.label42);
            this.tabPage7.Controls.Add(this.txtBox_CheckOut_Symptoms);
            this.tabPage7.Controls.Add(this.label43);
            this.tabPage7.Controls.Add(this.label44);
            this.tabPage7.Controls.Add(this.txtBox_CheckOut_Pulse);
            this.tabPage7.Controls.Add(this.txtBox_CheckOut_Temperature);
            this.tabPage7.Controls.Add(this.label45);
            this.tabPage7.Controls.Add(this.label46);
            this.tabPage7.Controls.Add(this.txtBox_CheckOut_DiastolicBP);
            this.tabPage7.Controls.Add(this.txtBox_CheckOut_SystolicBP);
            this.tabPage7.Controls.Add(this.txtBox_CheckOut_ReasonForVisit);
            this.tabPage7.Controls.Add(this.label47);
            this.tabPage7.Controls.Add(this.txtBox_CheckOut_FirstName);
            this.tabPage7.Controls.Add(this.txtBox_CheckOut_LastName);
            this.tabPage7.Controls.Add(this.label48);
            this.tabPage7.Location = new System.Drawing.Point(104, 4);
            this.tabPage7.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(483, 683);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Check-Out";
            // 
            // btn_CheckOut_Complete
            // 
            this.btn_CheckOut_Complete.Location = new System.Drawing.Point(208, 589);
            this.btn_CheckOut_Complete.Margin = new System.Windows.Forms.Padding(2);
            this.btn_CheckOut_Complete.Name = "btn_CheckOut_Complete";
            this.btn_CheckOut_Complete.Size = new System.Drawing.Size(115, 27);
            this.btn_CheckOut_Complete.TabIndex = 46;
            this.btn_CheckOut_Complete.Text = "Complete Check-Out";
            this.btn_CheckOut_Complete.UseVisualStyleBackColor = true;
            this.btn_CheckOut_Complete.Click += new System.EventHandler(this.btn_CheckOut_Complete_Click);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(90, 398);
            this.label41.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(53, 13);
            this.label41.TabIndex = 45;
            this.label41.Text = "Diagnosis";
            // 
            // txtBox_CheckOut_Diagnosis
            // 
            this.txtBox_CheckOut_Diagnosis.BackColor = System.Drawing.SystemColors.Window;
            this.txtBox_CheckOut_Diagnosis.Location = new System.Drawing.Point(92, 414);
            this.txtBox_CheckOut_Diagnosis.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_CheckOut_Diagnosis.Multiline = true;
            this.txtBox_CheckOut_Diagnosis.Name = "txtBox_CheckOut_Diagnosis";
            this.txtBox_CheckOut_Diagnosis.Size = new System.Drawing.Size(320, 112);
            this.txtBox_CheckOut_Diagnosis.TabIndex = 44;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(90, 261);
            this.label42.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(55, 13);
            this.label42.TabIndex = 43;
            this.label42.Text = "Symptoms";
            // 
            // txtBox_CheckOut_Symptoms
            // 
            this.txtBox_CheckOut_Symptoms.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBox_CheckOut_Symptoms.Enabled = false;
            this.txtBox_CheckOut_Symptoms.Location = new System.Drawing.Point(92, 276);
            this.txtBox_CheckOut_Symptoms.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_CheckOut_Symptoms.Multiline = true;
            this.txtBox_CheckOut_Symptoms.Name = "txtBox_CheckOut_Symptoms";
            this.txtBox_CheckOut_Symptoms.Size = new System.Drawing.Size(320, 90);
            this.txtBox_CheckOut_Symptoms.TabIndex = 42;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(357, 194);
            this.label43.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(33, 13);
            this.label43.TabIndex = 41;
            this.label43.Text = "Pulse";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(262, 194);
            this.label44.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(67, 13);
            this.label44.TabIndex = 40;
            this.label44.Text = "Temperature";
            // 
            // txtBox_CheckOut_Pulse
            // 
            this.txtBox_CheckOut_Pulse.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBox_CheckOut_Pulse.Enabled = false;
            this.txtBox_CheckOut_Pulse.Location = new System.Drawing.Point(359, 215);
            this.txtBox_CheckOut_Pulse.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_CheckOut_Pulse.Name = "txtBox_CheckOut_Pulse";
            this.txtBox_CheckOut_Pulse.Size = new System.Drawing.Size(53, 20);
            this.txtBox_CheckOut_Pulse.TabIndex = 39;
            // 
            // txtBox_CheckOut_Temperature
            // 
            this.txtBox_CheckOut_Temperature.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBox_CheckOut_Temperature.Enabled = false;
            this.txtBox_CheckOut_Temperature.Location = new System.Drawing.Point(271, 215);
            this.txtBox_CheckOut_Temperature.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_CheckOut_Temperature.Name = "txtBox_CheckOut_Temperature";
            this.txtBox_CheckOut_Temperature.Size = new System.Drawing.Size(53, 20);
            this.txtBox_CheckOut_Temperature.TabIndex = 38;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(173, 194);
            this.label45.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(64, 13);
            this.label45.TabIndex = 37;
            this.label45.Text = "Diastolic BP";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(90, 194);
            this.label46.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(60, 13);
            this.label46.TabIndex = 36;
            this.label46.Text = "Systolic BP";
            // 
            // txtBox_CheckOut_DiastolicBP
            // 
            this.txtBox_CheckOut_DiastolicBP.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBox_CheckOut_DiastolicBP.Enabled = false;
            this.txtBox_CheckOut_DiastolicBP.Location = new System.Drawing.Point(176, 215);
            this.txtBox_CheckOut_DiastolicBP.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_CheckOut_DiastolicBP.Name = "txtBox_CheckOut_DiastolicBP";
            this.txtBox_CheckOut_DiastolicBP.Size = new System.Drawing.Size(53, 20);
            this.txtBox_CheckOut_DiastolicBP.TabIndex = 35;
            // 
            // txtBox_CheckOut_SystolicBP
            // 
            this.txtBox_CheckOut_SystolicBP.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBox_CheckOut_SystolicBP.Enabled = false;
            this.txtBox_CheckOut_SystolicBP.Location = new System.Drawing.Point(92, 215);
            this.txtBox_CheckOut_SystolicBP.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_CheckOut_SystolicBP.Name = "txtBox_CheckOut_SystolicBP";
            this.txtBox_CheckOut_SystolicBP.Size = new System.Drawing.Size(53, 20);
            this.txtBox_CheckOut_SystolicBP.TabIndex = 34;
            // 
            // txtBox_CheckOut_ReasonForVisit
            // 
            this.txtBox_CheckOut_ReasonForVisit.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBox_CheckOut_ReasonForVisit.Enabled = false;
            this.txtBox_CheckOut_ReasonForVisit.Location = new System.Drawing.Point(92, 71);
            this.txtBox_CheckOut_ReasonForVisit.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_CheckOut_ReasonForVisit.Multiline = true;
            this.txtBox_CheckOut_ReasonForVisit.Name = "txtBox_CheckOut_ReasonForVisit";
            this.txtBox_CheckOut_ReasonForVisit.Size = new System.Drawing.Size(320, 112);
            this.txtBox_CheckOut_ReasonForVisit.TabIndex = 33;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(90, 55);
            this.label47.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(81, 13);
            this.label47.TabIndex = 32;
            this.label47.Text = "Reason for Visit";
            // 
            // txtBox_CheckOut_FirstName
            // 
            this.txtBox_CheckOut_FirstName.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBox_CheckOut_FirstName.Enabled = false;
            this.txtBox_CheckOut_FirstName.Location = new System.Drawing.Point(300, 21);
            this.txtBox_CheckOut_FirstName.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_CheckOut_FirstName.Name = "txtBox_CheckOut_FirstName";
            this.txtBox_CheckOut_FirstName.Size = new System.Drawing.Size(76, 20);
            this.txtBox_CheckOut_FirstName.TabIndex = 31;
            // 
            // txtBox_CheckOut_LastName
            // 
            this.txtBox_CheckOut_LastName.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBox_CheckOut_LastName.Enabled = false;
            this.txtBox_CheckOut_LastName.Location = new System.Drawing.Point(152, 21);
            this.txtBox_CheckOut_LastName.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_CheckOut_LastName.Name = "txtBox_CheckOut_LastName";
            this.txtBox_CheckOut_LastName.Size = new System.Drawing.Size(76, 20);
            this.txtBox_CheckOut_LastName.TabIndex = 30;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(28, 21);
            this.label48.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(59, 13);
            this.label48.TabIndex = 29;
            this.label48.Text = "Check-out:";
            // 
            // View_Main
            // 
            this.AcceptButton = this.btn_Search;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(622, 728);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "View_Main";
            this.Text = "HAP - Hospital Access Program";
            this.Load += new System.EventHandler(this.View_Main_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.testDBBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.ToolStripMenuItem loginToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem signInToolStripMenuItem;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_NewPatient;
        private System.Windows.Forms.Button btn_Search;
        private System.Windows.Forms.TextBox txtBox_Search2_LastName;
        private System.Windows.Forms.TextBox txtBox_Search1_FirstName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBox_Result_State;
        private System.Windows.Forms.TextBox txtBox_Result_City;
        private System.Windows.Forms.TextBox txtBox_Result_Phone;
        private System.Windows.Forms.TextBox txtBox_Result_Address1;
        private System.Windows.Forms.TextBox txtBox_Result_FirstName;
        private System.Windows.Forms.TextBox txtBox_Result_LastName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBox_PatientSelect;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btn_Clear;
        private System.Windows.Forms.Button btn_Update;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtBox_Appt_SymptomsAtCheckIn;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtBox_Appt_ReasonForAppointment;
        private System.Windows.Forms.TextBox txtBox_Appt_Doctor;
        private System.Windows.Forms.ComboBox comboBox_AppointmentSelect;
        private System.Windows.Forms.TextBox txtBox_Appt_FirstName;
        private System.Windows.Forms.TextBox txtBox_Appt_LastName;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtBox_Appt_Diagnosis;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtBox_Appt_Pulse;
        private System.Windows.Forms.TextBox txtBox_Appt_Temperature;
        private System.Windows.Forms.TextBox txtBox_Appt_Diastolic;
        private System.Windows.Forms.TextBox txtBox_Appt_Systolic;
        private System.Windows.Forms.Button btn_CreateNewAppointment;
        private System.Windows.Forms.Button btn_CheckIn_Clear;
        private System.Windows.Forms.Button btn_CheckIn_Complete;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtBox_CheckIn_Symptoms;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtBox_CheckIn_Pulse;
        private System.Windows.Forms.TextBox txtBox_CheckIn_Temperature;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtBox_CheckIn_Diastolic;
        private System.Windows.Forms.TextBox txtBox_CheckIn_Systolic;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtBox_CheckIn_ReasonForAppointment;
        private System.Windows.Forms.TextBox txtBox_CheckIn_FirstName;
        private System.Windows.Forms.TextBox txtBox_CheckIn_LastName;
        private System.Windows.Forms.DateTimePicker datePicker_Search2_DOB;
        private System.Windows.Forms.DateTimePicker datePicker_Search3_DOB;
        private System.Windows.Forms.TextBox txtBox_Result_SSN;
        private System.Windows.Forms.TextBox txtBox_Result_Gender;
        private System.Windows.Forms.ToolStripMenuItem currentUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem signOutToolStripMenuItem;
        private System.Windows.Forms.TextBox txtBox_Search1_LastName;
        private System.Windows.Forms.TextBox txtBox_Result_DOB;
        private System.Windows.Forms.TextBox txtBox_Result_Zip;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btn_UpdateAppointment;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btn_Order_Test;
        private System.Windows.Forms.ComboBox comboBox_Lab_Test;
        private System.Windows.Forms.TextBox txtBox_Lab_FirstName;
        private System.Windows.Forms.TextBox txtBox_Lab_LastName;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btn_CheckOut_Complete;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox txtBox_CheckOut_Diagnosis;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox txtBox_CheckOut_Symptoms;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox txtBox_CheckOut_Pulse;
        private System.Windows.Forms.TextBox txtBox_CheckOut_Temperature;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TextBox txtBox_CheckOut_DiastolicBP;
        private System.Windows.Forms.TextBox txtBox_CheckOut_SystolicBP;
        private System.Windows.Forms.TextBox txtBox_CheckOut_ReasonForVisit;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox txtBox_CheckOut_FirstName;
        private System.Windows.Forms.TextBox txtBox_CheckOut_LastName;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.ToolStripMenuItem adminToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem;
        private System.Windows.Forms.TextBox txtBox_ApptTime;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtBox_CheckIn_Time;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtBox_CheckIn_Date;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox txtBox_Lab_ApptDate;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.BindingSource testDBBindingSource;
        private System.Windows.Forms.ListBox lstBox_Lab_TestsOrdered;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Button btn_Lab_EnterResult;
    }
}