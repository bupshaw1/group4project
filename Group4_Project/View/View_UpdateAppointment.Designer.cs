﻿namespace Group4_Project.View
{
    partial class View_UpdateAppointment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtBox_LastName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBox_FirstName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dateBox_Date = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox_Time = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBox_Doctor = new System.Windows.Forms.ComboBox();
            this.txtBox_ReasonForAppointment = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btn_Update = new System.Windows.Forms.Button();
            this.btn_Clear = new System.Windows.Forms.Button();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(162, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Appointment Details";
            // 
            // txtBox_LastName
            // 
            this.txtBox_LastName.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBox_LastName.Enabled = false;
            this.txtBox_LastName.Location = new System.Drawing.Point(103, 42);
            this.txtBox_LastName.Name = "txtBox_LastName";
            this.txtBox_LastName.Size = new System.Drawing.Size(100, 22);
            this.txtBox_LastName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Last Name :";
            // 
            // txtBox_FirstName
            // 
            this.txtBox_FirstName.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBox_FirstName.Enabled = false;
            this.txtBox_FirstName.Location = new System.Drawing.Point(322, 42);
            this.txtBox_FirstName.Name = "txtBox_FirstName";
            this.txtBox_FirstName.Size = new System.Drawing.Size(100, 22);
            this.txtBox_FirstName.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(232, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "First Name :";
            // 
            // dateBox_Date
            // 
            this.dateBox_Date.Location = new System.Drawing.Point(103, 101);
            this.dateBox_Date.Name = "dateBox_Date";
            this.dateBox_Date.Size = new System.Drawing.Size(245, 22);
            this.dateBox_Date.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Date :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 156);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 17);
            this.label5.TabIndex = 7;
            this.label5.Text = "Time :";
            // 
            // comboBox_Time
            // 
            this.comboBox_Time.AccessibleRole = System.Windows.Forms.AccessibleRole.ScrollBar;
            this.comboBox_Time.FormattingEnabled = true;
            this.comboBox_Time.Items.AddRange(new object[] {
            "9:00 AM",
            "9:30 AM",
            "10:00 AM",
            "10:30 AM",
            "11:00 AM",
            "11:30 AM",
            "1:00 PM",
            "1:30 PM",
            "2:00 PM",
            "2:30 PM",
            "3:00 PM",
            "3:30 PM"});
            this.comboBox_Time.Location = new System.Drawing.Point(103, 156);
            this.comboBox_Time.Name = "comboBox_Time";
            this.comboBox_Time.Size = new System.Drawing.Size(121, 24);
            this.comboBox_Time.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 211);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 17);
            this.label6.TabIndex = 9;
            this.label6.Text = "Doctor :";
            // 
            // comboBox_Doctor
            // 
            this.comboBox_Doctor.FormattingEnabled = true;
            this.comboBox_Doctor.Location = new System.Drawing.Point(103, 211);
            this.comboBox_Doctor.Name = "comboBox_Doctor";
            this.comboBox_Doctor.Size = new System.Drawing.Size(294, 24);
            this.comboBox_Doctor.TabIndex = 10;
            // 
            // txtBox_ReasonForAppointment
            // 
            this.txtBox_ReasonForAppointment.HideSelection = false;
            this.txtBox_ReasonForAppointment.Location = new System.Drawing.Point(103, 297);
            this.txtBox_ReasonForAppointment.Multiline = true;
            this.txtBox_ReasonForAppointment.Name = "txtBox_ReasonForAppointment";
            this.txtBox_ReasonForAppointment.Size = new System.Drawing.Size(319, 212);
            this.txtBox_ReasonForAppointment.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 277);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(169, 17);
            this.label7.TabIndex = 12;
            this.label7.Text = "Reason for Appointment :";
            // 
            // btn_Update
            // 
            this.btn_Update.Location = new System.Drawing.Point(103, 546);
            this.btn_Update.Name = "btn_Update";
            this.btn_Update.Size = new System.Drawing.Size(75, 26);
            this.btn_Update.TabIndex = 13;
            this.btn_Update.Text = "Update";
            this.btn_Update.UseVisualStyleBackColor = true;
            this.btn_Update.Click += new System.EventHandler(this.btn_Update_Click);
            // 
            // btn_Clear
            // 
            this.btn_Clear.Location = new System.Drawing.Point(209, 546);
            this.btn_Clear.Name = "btn_Clear";
            this.btn_Clear.Size = new System.Drawing.Size(75, 26);
            this.btn_Clear.TabIndex = 14;
            this.btn_Clear.Text = "Clear";
            this.btn_Clear.UseVisualStyleBackColor = true;
            this.btn_Clear.Click += new System.EventHandler(this.btn_Clear_Click);
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(322, 546);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(75, 26);
            this.btn_Cancel.TabIndex = 15;
            this.btn_Cancel.Text = "Cancel";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // View_UpdateAppointment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(462, 595);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_Clear);
            this.Controls.Add(this.btn_Update);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtBox_ReasonForAppointment);
            this.Controls.Add(this.comboBox_Doctor);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.comboBox_Time);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dateBox_Date);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtBox_FirstName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtBox_LastName);
            this.Controls.Add(this.label1);
            this.Name = "View_UpdateAppointment";
            this.Text = "Appointment";
            this.Load += new System.EventHandler(this.View_UpdateAppointment_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBox_LastName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBox_FirstName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dateBox_Date;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBox_Time;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBox_Doctor;
        private System.Windows.Forms.TextBox txtBox_ReasonForAppointment;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btn_Update;
        private System.Windows.Forms.Button btn_Clear;
        private System.Windows.Forms.Button btn_Cancel;
    }
}