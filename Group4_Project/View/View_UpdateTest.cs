﻿using Group4_Project.Controller;
using Group4_Project.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Group4_Project.View
{
    public partial class View_UpdateTest : Form
    {
        private Group4Controller inController;
        private Patient currPatient;
        private Appointment currAppointment;
        private Test currTest;

        public View_UpdateTest()
        {
            InitializeComponent();
        }

        public View_UpdateTest(int testPerformedID, int patientID, int apptID)
        {
            InitializeComponent();
            inController = new Controller.Group4Controller();
            currTest = inController.GetTest(testPerformedID);
            currPatient = inController.GetPatient(patientID);
            currAppointment = inController.GetAppointment(apptID);
            lbl_UpdTest_Patient.Text = currPatient.lname + ", " + currPatient.fname;
            lbl_UpdTest_ApptDate.Text = currAppointment.ApptDate.ToShortDateString();
            lbl_UpdTest_Name.Text = currTest.TestName;
            dat_UpdTest_DatePerformed.Value = DateTime.Today;
            rad_Normal.Checked = true;
            rad_Abnormal.Checked = false;
        }

        private bool isValidTest()
        {
            string message = "";
            if (dat_UpdTest_DatePerformed == null || dat_UpdTest_DatePerformed.Value > DateTime.Today)
                message += "Performed date cannot be empty or later than today\n";
            if (!rad_Abnormal.Checked && !rad_Normal.Checked)
                message += "Result must be picked\n";
            if (message != "")
                MessageBox.Show(message);
            return (message == "");
        }

        private void btn_UpdTest_EnterResults_Click(object sender, EventArgs e)
        {
            if(isValidTest())
            {
                try
                {
                    string results;
                    if (rad_Normal.Checked)
                        results = "Normal";
                    else
                        results = "Abnormal";
                    string message = "You are about to update this test with\n";
                    message += "DatePerformed: " + dat_UpdTest_DatePerformed.Value.ToShortDateString() + "\n";
                    message += "Results: " + results;
                    DialogResult result = MessageBox.Show(message, "Confirmation", MessageBoxButtons.YesNo);
                    if(result == DialogResult.Yes)
                    {
                        currTest.DatePerformed = dat_UpdTest_DatePerformed.Value;
                        if(rad_Normal.Checked)
                        {
                            currTest.ResultsNormal = true;
                        }
                        else
                        {
                            currTest.ResultsNormal = false;
                        }
                        if(inController.EnterTestResults(currTest))
                        {
                            MessageBox.Show("Test has been updated");
                            Close();
                            (System.Windows.Forms.Application.OpenForms["View_Main"] as View_Main).PopulateTestsOrdered(currTest.VisitID);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Updated was aborted");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
            else
            {
                MessageBox.Show("Invalid test results - nothing updated");
            }
        }
    }
}
