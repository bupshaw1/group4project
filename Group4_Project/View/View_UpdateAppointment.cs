﻿using Group4_Project.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Group4_Project.Controller;

namespace Group4_Project.View
{
    public partial class View_UpdateAppointment : Form
    {
        private Group4Controller inController;
        private Patient currPatient;
        private List<Doctor> doctorList;
        private Appointment appointment;
        private Doctor selectedDoctor;

        public View_UpdateAppointment()
        {
            InitializeComponent();
        }

        public View_UpdateAppointment(int patientID)
        {
            InitializeComponent();
            inController = new Controller.Group4Controller();
            currPatient = inController.GetPatient(patientID);
            doctorList = inController.GetDoctorList();
            txtBox_LastName.Text = currPatient.lname;
            txtBox_FirstName.Text = currPatient.fname;
            populateDoctorList();
        }

        public View_UpdateAppointment(int appointmentID, int patientID)
        {
            InitializeComponent();
            inController = new Controller.Group4Controller();
            currPatient = inController.GetPatient(patientID);
            doctorList = inController.GetDoctorList();
            appointment = inController.GetAppointment(appointmentID);
            txtBox_LastName.Text = currPatient.lname;
            txtBox_FirstName.Text = currPatient.fname;
            txtBox_ReasonForAppointment.Text = appointment.ReasonForVisit;
            dateBox_Date.Value = appointment.ApptDate;
            comboBox_Time.SelectedIndex = convertApptTime(appointment);
            populateDoctorList();
        }


        private void View_UpdateAppointment_Load(object sender, EventArgs e)
        {

        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_Clear_Click(object sender, EventArgs e)
        {
            txtBox_FirstName.Clear();
            txtBox_LastName.Clear();
            dateBox_Date.Refresh();
            comboBox_Time.SelectedIndex = -1;
            txtBox_ReasonForAppointment.Clear();
            populateDoctorList();
        }

        private void populateDoctorList()
        {
            comboBox_Doctor.DataSource = doctorList.Distinct().ToList();
            comboBox_Doctor.DisplayMember = "displayFullName";
            comboBox_Doctor.ValueMember = "doctorID";
        }

        private bool isValidAppointment()
        {
            string message = "";
            if (txtBox_FirstName.Text == "")
                message += "First name cannot be empty\n";
            if (txtBox_LastName.Text == "")
                message += "Last name cannot be empty\n";
            if (dateBox_Date.Value <= DateTime.Today)
                message += "Appointment date must be later than today\n";
            if (comboBox_Doctor.SelectedIndex == -1)
                message += "Doctor is required\n";
            if (comboBox_Time.SelectedIndex == -1)
                message += "Time is required\n";
            if (txtBox_ReasonForAppointment.Text == "")
                message += "Reason for Appointment is required ";
            if (message != "")
                MessageBox.Show(message);
            return (message == "");
        }

        private void btn_Update_Click(object sender, EventArgs e)
        {
            if (appointment == null)  // New appointment
            {
                if (isValidAppointment())
                {
                    appointment = new Appointment();
                    this.PutAppointmentData(appointment);
                    try
                    {
                        this.inController.AddAppointment(appointment);
                        this.DialogResult = DialogResult.OK;
                        MessageBox.Show("Appointment added successfully.");
                        Close();
                        (System.Windows.Forms.Application.OpenForms["View_Main"] as View_Main).loadAppointments(appointment.PatientID);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, ex.GetType().ToString());
                    }
                }
            }
            else // Update appointment
            {
                if (isValidAppointment())
                {
                    //appointment = new Appointment();
                    this.PutAppointmentData(appointment);
                    try
                    {
                        this.inController.UpdateAppointment(appointment);
                        this.DialogResult = DialogResult.OK;
                        MessageBox.Show("Appointment updated successfully.");
                        Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, ex.GetType().ToString());
                    }
                }
            }
            
        }

        private void PutAppointmentData(Appointment appointment)
        {
            this.selectedDoctor = doctorList[comboBox_Doctor.SelectedIndex];
            appointment.ApptDate = dateBox_Date.Value;
            appointment.ApptTime = comboBox_Time.Text;
            appointment.ReasonForVisit = txtBox_ReasonForAppointment.Text;
            appointment.PatientID = currPatient.patientID;
            appointment.DoctorID = selectedDoctor.doctorID;
            appointment.DoctorFullName = "";
        }

        private int convertApptTime(Appointment appt)
        {
            string apptTime = appt.ApptTime;
            int selectedIndex = -1;

            switch (apptTime)
            {
                case "09:00:00":
                    selectedIndex = 0;
                    break;
                case "09:30:00":
                    selectedIndex = 1;
                    break;
                case "10:00:00":
                    selectedIndex = 2;
                    break;
                case "10:30:00":
                    selectedIndex = 3;
                    break;
                case "11:00:00":
                    selectedIndex = 4;
                    break;
                case "11:30:00":
                    selectedIndex = 5;
                    break;
                case "13:00:00":
                    selectedIndex = 6;
                    break;
                case "13:30:00":
                    selectedIndex = 7;
                    break;
                case "14:00:00":
                    selectedIndex = 8;
                    break;
                case "14:30:00":
                    selectedIndex = 9;
                    break;
                case "15:00:00":
                    selectedIndex = 10;
                    break;
                case "15:30:00":
                    selectedIndex = 11;
                    break;

                default:
                    selectedIndex = 2;
                    break;
            }
            return selectedIndex;
        }

    }
}
