﻿namespace Group4_Project.View
{
    partial class View_NewPatient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtBox_FirstName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBox_LastName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBox_Address1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBox_Phone = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBox_City = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBox_State = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtBox_Zip = new System.Windows.Forms.TextBox();
            this.btn_AddPatient = new System.Windows.Forms.Button();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtBox_Gender = new System.Windows.Forms.TextBox();
            this.txtBox_SSN = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtBox_DoB = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(118, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "New Patient";
            // 
            // txtBox_FirstName
            // 
            this.txtBox_FirstName.Location = new System.Drawing.Point(76, 59);
            this.txtBox_FirstName.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_FirstName.Name = "txtBox_FirstName";
            this.txtBox_FirstName.Size = new System.Drawing.Size(98, 20);
            this.txtBox_FirstName.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 62);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "First Name :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(188, 62);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Last Name :";
            // 
            // txtBox_LastName
            // 
            this.txtBox_LastName.Location = new System.Drawing.Point(255, 59);
            this.txtBox_LastName.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_LastName.Name = "txtBox_LastName";
            this.txtBox_LastName.Size = new System.Drawing.Size(98, 20);
            this.txtBox_LastName.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 176);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Address :";
            // 
            // txtBox_Address1
            // 
            this.txtBox_Address1.Location = new System.Drawing.Point(76, 176);
            this.txtBox_Address1.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_Address1.Name = "txtBox_Address1";
            this.txtBox_Address1.Size = new System.Drawing.Size(136, 20);
            this.txtBox_Address1.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 290);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Phone :";
            // 
            // txtBox_Phone
            // 
            this.txtBox_Phone.Location = new System.Drawing.Point(76, 290);
            this.txtBox_Phone.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_Phone.MaxLength = 14;
            this.txtBox_Phone.Name = "txtBox_Phone";
            this.txtBox_Phone.Size = new System.Drawing.Size(98, 20);
            this.txtBox_Phone.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 214);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "City :";
            // 
            // txtBox_City
            // 
            this.txtBox_City.Location = new System.Drawing.Point(76, 214);
            this.txtBox_City.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_City.Name = "txtBox_City";
            this.txtBox_City.Size = new System.Drawing.Size(98, 20);
            this.txtBox_City.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 252);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "State :";
            // 
            // comboBox_State
            // 
            this.comboBox_State.FormattingEnabled = true;
            this.comboBox_State.Location = new System.Drawing.Point(76, 252);
            this.comboBox_State.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox_State.Name = "comboBox_State";
            this.comboBox_State.Size = new System.Drawing.Size(98, 21);
            this.comboBox_State.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(200, 252);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Zip :";
            // 
            // txtBox_Zip
            // 
            this.txtBox_Zip.Location = new System.Drawing.Point(231, 252);
            this.txtBox_Zip.Margin = new System.Windows.Forms.Padding(2);
            this.txtBox_Zip.MaxLength = 10;
            this.txtBox_Zip.Name = "txtBox_Zip";
            this.txtBox_Zip.Size = new System.Drawing.Size(71, 20);
            this.txtBox_Zip.TabIndex = 10;
            // 
            // btn_AddPatient
            // 
            this.btn_AddPatient.Location = new System.Drawing.Point(76, 332);
            this.btn_AddPatient.Margin = new System.Windows.Forms.Padding(2);
            this.btn_AddPatient.Name = "btn_AddPatient";
            this.btn_AddPatient.Size = new System.Drawing.Size(79, 25);
            this.btn_AddPatient.TabIndex = 12;
            this.btn_AddPatient.Text = "Add Patient";
            this.btn_AddPatient.UseVisualStyleBackColor = true;
            this.btn_AddPatient.Click += new System.EventHandler(this.btn_AddPatient_Click);
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(190, 332);
            this.btn_Cancel.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(79, 25);
            this.btn_Cancel.TabIndex = 13;
            this.btn_Cancel.Text = "Cancel";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 100);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Gender (M/F):";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 138);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 13);
            this.label10.TabIndex = 20;
            this.label10.Text = "SSN:";
            // 
            // txtBox_Gender
            // 
            this.txtBox_Gender.Location = new System.Drawing.Point(89, 97);
            this.txtBox_Gender.MaxLength = 1;
            this.txtBox_Gender.Name = "txtBox_Gender";
            this.txtBox_Gender.Size = new System.Drawing.Size(30, 20);
            this.txtBox_Gender.TabIndex = 4;
            // 
            // txtBox_SSN
            // 
            this.txtBox_SSN.Location = new System.Drawing.Point(76, 138);
            this.txtBox_SSN.MaxLength = 9;
            this.txtBox_SSN.Name = "txtBox_SSN";
            this.txtBox_SSN.Size = new System.Drawing.Size(100, 20);
            this.txtBox_SSN.TabIndex = 6;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(188, 100);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(31, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "DoB:";
            // 
            // txtBox_DoB
            // 
            this.txtBox_DoB.Location = new System.Drawing.Point(255, 100);
            this.txtBox_DoB.MaxLength = 10;
            this.txtBox_DoB.Name = "txtBox_DoB";
            this.txtBox_DoB.Size = new System.Drawing.Size(100, 20);
            this.txtBox_DoB.TabIndex = 5;
            // 
            // View_NewPatient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(366, 435);
            this.Controls.Add(this.txtBox_DoB);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtBox_SSN);
            this.Controls.Add(this.txtBox_Gender);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_AddPatient);
            this.Controls.Add(this.txtBox_Zip);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.comboBox_State);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtBox_City);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtBox_Phone);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtBox_Address1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtBox_LastName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtBox_FirstName);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "View_NewPatient";
            this.Text = "New Patient";
            this.Load += new System.EventHandler(this.View_NewPatient_Load_1);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBox_FirstName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBox_LastName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBox_Address1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtBox_Phone;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtBox_City;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBox_State;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtBox_Zip;
        private System.Windows.Forms.Button btn_AddPatient;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtBox_Gender;
        private System.Windows.Forms.TextBox txtBox_SSN;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtBox_DoB;
    }
}