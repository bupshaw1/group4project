﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Group4_Project.Controller;
using Group4_Project.Model;
using Group4_Project.DAL;
using System.Data.SqlClient;

namespace Group4_Project.View
{
    public partial class View_ReportTool : Form
    {
        private Group4Controller inController;

        public View_ReportTool()
        {
            InitializeComponent();
            inController = new Group4Controller();
        }

        private void View_ReportTool_Load(object sender, EventArgs e)
        {
            this.reportViewer1.RefreshReport();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btn_Run_Report_Click(object sender, EventArgs e)
        {
            if (IsValidData())
            {
                sp_mostPerformedTestsDuringDatesTableAdapter.Fill(_CS6232_g4DataSet.sp_mostPerformedTestsDuringDates, txtBox_Start_Date.Text.ToString(), txtBox_End_Date.Text.ToString());
                reportViewer1.RefreshReport();
            }
        }

        private bool IsValidData()
        {
            if (Validator.IsPresent(txtBox_Start_Date) &&
                Validator.IsPresent(txtBox_End_Date) &&
                Validator.IsDate(txtBox_Start_Date.Text.ToString()) &&
                Validator.IsDate(txtBox_End_Date.Text.ToString()) &&
                Validator.IsStartDateBeforeEndDate(txtBox_Start_Date.Text.ToString(), txtBox_End_Date.Text.ToString()))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
