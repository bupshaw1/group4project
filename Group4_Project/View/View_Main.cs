﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Group4_Project.Controller;
using Group4_Project.Model;
using Group4_Project.DAL;
using System.Text.RegularExpressions;

namespace Group4_Project.View
{
    public partial class View_Main : Form
    {
        private List<TabPage> hiddenPages = new List<TabPage>();
        private Group4Controller inController;
        private List<Patient> patientList;
        private List<Appointment> appointmentList;
        private List<Test> testList;
        private Test test;
        private Patient patient;
        private Patient newPatient;
        private Patient updateNewPatient;
        private Patient selectedPatient;
        private Appointment selectedAppointment;
        private Visit visit;
        private List<State> stateList;
        private Boolean searchEnabled;
        private string loginID;
        private Boolean adminFileMenu = false;

        public View_Main()
        {
            InitializeComponent();

            tabControl1.DrawItem += new DrawItemEventHandler(tabControl1_DrawItem);
            this.EnablePage(tabPage1, false);
            this.EnablePage(tabPage2, false);
            this.EnablePage(tabPage4, false);
            this.EnablePage(tabPage5, false);
            this.EnablePage(tabPage7, false);

            inController = new Group4Controller();
            newPatient = new Patient();
            patient = new Patient();
            updateNewPatient = new Patient();
            searchEnabled = false;
        }

        private void tabControl1_DrawItem(Object sender, System.Windows.Forms.DrawItemEventArgs e)
        {
            Graphics g = e.Graphics;
            Brush _textBrush;

            TabPage _tabPage = tabControl1.TabPages[e.Index];

            Rectangle _tabBounds = tabControl1.GetTabRect(e.Index);

            if (e.State == DrawItemState.Selected)
            {
                _textBrush = new SolidBrush(Color.Blue);
                g.FillRectangle(Brushes.Gray, e.Bounds);
            }
            else
            {
                _textBrush = new System.Drawing.SolidBrush(e.ForeColor);
                e.DrawBackground();
            }
            Font _tabFont = new Font("Arial", (float)10.0, FontStyle.Bold, GraphicsUnit.Pixel);

            StringFormat _stringsFlags = new StringFormat();
            _stringsFlags.Alignment = StringAlignment.Center;
            _stringsFlags.LineAlignment = StringAlignment.Center;
            g.DrawString(_tabPage.Text, _tabFont, _textBrush, _tabBounds, new StringFormat(_stringsFlags));
        }

        private void EnablePage(TabPage page, bool enable)
        {
            if (enable)
            {
                tabControl1.TabPages.Add(page);
                hiddenPages.Remove(page);
            }
            else
            {
                tabControl1.TabPages.Remove(page);
                hiddenPages.Add(page);
            }
        }

        public void EnableTab1()
        {
            this.EnablePage(tabPage1, true);
        }

        public void EnableAdminFileMenu()
        {
            this.adminToolStripMenuItem.Visible = true;
            adminFileMenu = true;
        }

        public void SuccessfulSignIn(string cur_user)
        {
            loginID = cur_user;
            this.signOutToolStripMenuItem.Visible = true;
            this.currentUserToolStripMenuItem.Visible = true;
            this.currentUserToolStripMenuItem.Text = "Current User: " + cur_user;     
            this.signInToolStripMenuItem.Visible = false;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void signInToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new View_SignIn().Show();
        }

        private void LoadComboBoxes()
        {
            try
            {
                stateList = this.inController.GetStateList();
                comboBox_Result_State.DataSource = stateList;
                comboBox_Result_State.DisplayMember = "StateID";
                comboBox_Result_State.ValueMember = "StateID";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            foreach (var page in hiddenPages) page.Dispose();
            base.OnFormClosed(e);
        }

        private void btn_Search_Click(object sender, EventArgs e)
        {

            if (txtBox_Search1_FirstName.Text == "" && txtBox_Search1_LastName.Text == "")
            {
                if (txtBox_Search2_LastName.Text == "" && datePicker_Search2_DOB.Value == datePicker_Search2_DOB.MinDate)
                {
                    if (datePicker_Search3_DOB.Value == datePicker_Search3_DOB.MinDate)
                    {
                        MessageBox.Show("Invalid Search!  All search fields are empty!");
                    }
                    else
                    {
                        patientList = this.SearchByDOB(datePicker_Search3_DOB.Value.Date);
                        this.loadPatientList();
                        if (searchEnabled == false)
                        {
                            searchEnabled = true;
                        }
                    }
                }
                else
                {
                    patientList = this.SearchByLastAndDOB((txtBox_Search2_LastName.Text), (datePicker_Search2_DOB.Value.Date));
                    this.loadPatientList();
                    if (searchEnabled == false)
                    {
                        searchEnabled = true;
                    }
                }
            }
            else
            {
                patientList = this.SearchByName((txtBox_Search1_FirstName.Text), (txtBox_Search1_LastName.Text));
                this.loadPatientList();
                if (searchEnabled == false)
                {
                    searchEnabled = true;
                }
            }
        }
        
        private void loadPatientList()
        {
            Patient patientSelect = new Patient();
            patientSelect.displayFullName = "Select Patient";
            patientSelect.patientID = 0;
            patientList.Insert(0, patientSelect);

            comboBox_PatientSelect.DataSource = patientList.Distinct().ToList();
            comboBox_PatientSelect.DisplayMember = "displayFullName";
            comboBox_PatientSelect.ValueMember = "patientID";
        }

        private void comboBox_PatientSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.selectedPatient = patientList[comboBox_PatientSelect.SelectedIndex];
            if (this.selectedPatient.patientID > 0)
            {
                this.loadSearchResults(selectedPatient); //loads patient fields
                this.getPatientAppointments(selectedPatient.patientID); //populates appointment drop down list
                this.EnablePage(tabPage4, false);
                this.EnablePage(tabPage7, false);
                this.EnablePage(tabPage5, false);
                patient.personID = selectedPatient.personID;
                patient.lname = selectedPatient.lname;
                patient.fname = selectedPatient.fname;
                patient.gender = selectedPatient.gender;
                patient.ssn = selectedPatient.ssn;
                patient.address = selectedPatient.address;
                patient.phone = selectedPatient.phone;
                patient.city = selectedPatient.city;
                patient.state = selectedPatient.state;
                patient.dob = selectedPatient.dob;
                patient.zip = selectedPatient.zip;
                this.PutNewPatient();
            }
            else
            {
                this.EnablePage(tabPage2, false);
                this.EnablePage(tabPage4, false);
                this.EnablePage(tabPage7, false);
                this.EnablePage(tabPage5, false);
                txtBox_Result_LastName.Text = "Last Name";
                txtBox_Result_FirstName.Text = "First Name";
                txtBox_Result_Gender.Text = "Gender";
                txtBox_Result_SSN.Text = "SSN";
                txtBox_Result_Address1.Text = "Address";
                txtBox_Result_Phone.Text = "Phone Number";
                txtBox_Result_City.Text = "City";
                comboBox_Result_State.Text = "AZ";
                txtBox_Result_DOB.Text = "DoB";
                txtBox_Result_Zip.Text = "Zip";
            }
        }
        
        private void loadSearchResults(Patient selectedPatient)
        {
            txtBox_Result_LastName.Text = selectedPatient.lname;
            txtBox_Result_FirstName.Text = selectedPatient.fname;
            txtBox_Result_Gender.Text = selectedPatient.gender;
            txtBox_Result_SSN.Text = selectedPatient.ssn;
            txtBox_Result_Address1.Text = selectedPatient.address;
            txtBox_Result_Phone.Text = selectedPatient.phone;
            txtBox_Result_City.Text = selectedPatient.city;
            comboBox_Result_State.Text = selectedPatient.state;
            txtBox_Result_DOB.Text = selectedPatient.dob.Date.ToString("yyyy-MM-dd");
            txtBox_Result_Zip.Text = Regex.Replace(selectedPatient.zip, "\\s+", "");
            txtBox_Appt_FirstName.Text = selectedPatient.fname;
            txtBox_Appt_LastName.Text = selectedPatient.lname;
            txtBox_CheckIn_LastName.Text = selectedPatient.lname;
            txtBox_CheckIn_FirstName.Text = selectedPatient.fname;
        }

        private void resetSearchResults()
        {
            txtBox_Result_LastName.Text = "Last Name";
            txtBox_Result_FirstName.Text = "First Name";
            txtBox_Result_Gender.Text = "Gender";
            txtBox_Result_SSN.Text = "SSN";
            txtBox_Result_Address1.Text = "Address";
            txtBox_Result_Phone.Text = "Phone Number";
            txtBox_Result_City.Text = "City";
            comboBox_Result_State.Text = "AK";
            txtBox_Result_DOB.Text = "DoB";
            txtBox_Result_Zip.Text = "Zip";
            btn_Search.PerformClick();
        }

        public void getPatientAppointments(int patientID)
        {
            appointmentList = inController.GetPatientAppointments(patientID);

            if (comboBox_PatientSelect.SelectedIndex > 0)
            {
                loadAppointments(patientID);
            }
            else
            {
                this.EnablePage(tabPage2, false);
            }
        }

        public void loadAppointments(int patientID)
        {
            if(selectedPatient != null)
            {
                this.EnablePage(tabPage2, false);
                this.EnablePage(tabPage2, true);
            }
            appointmentList = inController.GetPatientAppointments(patientID);
            Appointment appointmentSelect = new Appointment();
            appointmentSelect.DisplayFullAppt = "Select Appointment";
            appointmentSelect.ApptID = 0;
            appointmentList.Insert(0, appointmentSelect);
                        
            comboBox_AppointmentSelect.DataSource = appointmentList.Distinct().ToList();
            comboBox_AppointmentSelect.DisplayMember = "DisplayFullAppt";
            comboBox_AppointmentSelect.ValueMember = "ApptID";
        }

        private void loadAppointmentResults(Appointment appointment)
        {
            txtBox_Appt_Doctor.Text = appointment.DoctorFullName;
            txtBox_Appt_ReasonForAppointment.Text = appointment.ReasonForVisit;
            txtBox_ApptTime.Text = appointment.ApptTime;
        }

        private void loadVisitResults(int apptID)
        {
            visit = inController.GetPatientVisit(apptID);
            txtBox_Appt_SymptomsAtCheckIn.Text = visit.symptoms;
            txtBox_Appt_Systolic.Text = visit.bpSystolic.ToString();
            txtBox_Appt_Diastolic.Text = visit.bpDiastolic.ToString();
            txtBox_Appt_Temperature.Text = visit.temperature.ToString();
            txtBox_Appt_Pulse.Text = visit.pulse.ToString();
            txtBox_Appt_Diagnosis.Text = visit.diagnosis;
            txtBox_CheckIn_Date.Text = appointmentList[comboBox_AppointmentSelect.SelectedIndex].ApptDate.ToShortDateString();
            txtBox_CheckIn_Time.Text = appointmentList[comboBox_AppointmentSelect.SelectedIndex].ApptTime;
            txtBox_CheckIn_ReasonForAppointment.Text = appointmentList[comboBox_AppointmentSelect.SelectedIndex].ReasonForVisit;
        }

        private void loadLabTests()
        {
            txtBox_Lab_LastName.Text = selectedPatient.lname;
            txtBox_Lab_FirstName.Text = selectedPatient.fname;
            txtBox_Lab_ApptDate.Text = appointmentList[comboBox_AppointmentSelect.SelectedIndex].DisplayFullAppt;
            this.getAvailableTests();

            Test testSelect = new Test();
            testSelect.TestName = "Select Test";
            testSelect.TestID = 0;
            testList.Insert(0, testSelect);

            comboBox_Lab_Test.DataSource = testList.Distinct().ToList();
            comboBox_Lab_Test.DisplayMember = "TestName";
            comboBox_Lab_Test.ValueMember = "TestID";

            PopulateTestsOrdered(visit.visitID);
        }

        private void comboBox_AppointmentSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.EnablePage(tabPage4, false);
            this.EnablePage(tabPage7, false);
            this.EnablePage(tabPage5, false);
            selectedAppointment = appointmentList[comboBox_AppointmentSelect.SelectedIndex];
            this.loadAppointmentResults(selectedAppointment);
            this.loadVisitResults(selectedAppointment.ApptID);  //loads visit info on Appt tab  
            this.loadLabTests();
            this.loadCheckOut();
            if (txtBox_Appt_SymptomsAtCheckIn.Text == "")
            {
                this.EnablePage(tabPage4, true);
            }
            else
            {
                if (txtBox_Appt_Diagnosis.Text == "")
                {
                    this.EnablePage(tabPage5, true);
                    this.EnablePage(tabPage7, true);
                }
                else
                {
                    this.EnablePage(tabPage5, true);
                }
                
            }
        }

        private List<Patient> SearchByName(string first, string last)
        {
            return this.inController.SearchByName(last, first);
        }

        private List<Patient> SearchByLastAndDOB(string last, DateTime dob)
        {
            return this.inController.SearchByLastAndDOB(last, dob);
        }

        private List<Patient> SearchByDOB(DateTime dob)
        {
            return this.inController.SearchByDOB(dob);
        }

        private void btn_NewPatient_Click(object sender, EventArgs e)
        {
            new View_NewPatient().Show();
        }

        private void signOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.signInToolStripMenuItem.Visible = true;
            this.signOutToolStripMenuItem.Visible = false;
            this.currentUserToolStripMenuItem.Visible = false;
            this.adminToolStripMenuItem.Visible = false;
            this.EnablePage(tabPage1, false);
            this.EnablePage(tabPage2, false);
            this.EnablePage(tabPage4, false);
            this.EnablePage(tabPage5, false);
            this.EnablePage(tabPage7, false);
            adminFileMenu = false;
        }

        private void btn_Update_Click(object sender, EventArgs e)
        {
            if (comboBox_PatientSelect.SelectedIndex != 0)
            {
                if (IsValidDataPatientUpdate())
                {
                    try
                    {
                        if (!PatientDB.UpdatePatientValidate(patient, newPatient))
                        {
                            MessageBox.Show("Another user has updated or deleted that patient. Please redo your search.", "Database Error");
                            this.DialogResult = DialogResult.Retry;
                        }
                        else
                        {
                            this.UpdateNewPatient();
                            PatientDB.UpdatePatient(updateNewPatient);
                            MessageBox.Show("Patient has been updated. Please search again.", "Update successful");
                            patient = newPatient;
                            newPatient = new Patient();
                            this.DialogResult = DialogResult.OK;
                            this.Clear();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, ex.GetType().ToString());
                    }
                }
            }
            else
            {
                MessageBox.Show("Please select a patient.");
            }
        }

        private void PutNewPatient()
        {
            newPatient.personID = patient.personID;
            newPatient.fname = patient.fname;
            newPatient.lname = patient.lname;
            newPatient.dob = patient.dob;
            newPatient.ssn = patient.ssn;
            newPatient.gender = patient.gender;
            newPatient.address = patient.address;
            newPatient.city = patient.city;
            newPatient.state = patient.state;
            newPatient.zip = patient.zip;
            newPatient.phone = patient.phone;
        }

        private void UpdateNewPatient()
        {
            updateNewPatient.personID = patient.personID;
            updateNewPatient.lname = txtBox_Result_LastName.Text;
            updateNewPatient.fname = txtBox_Result_FirstName.Text;
            updateNewPatient.gender = txtBox_Result_Gender.Text;
            updateNewPatient.ssn = txtBox_Result_SSN.Text;
            updateNewPatient.address = txtBox_Result_Address1.Text;
            updateNewPatient.phone = txtBox_Result_Phone.Text;
            updateNewPatient.city = txtBox_Result_City.Text;
            updateNewPatient.state = comboBox_Result_State.Text;
            updateNewPatient.dob = Convert.ToDateTime(txtBox_Result_DOB.Text);
            updateNewPatient.zip = txtBox_Result_Zip.Text;
        }

        private void btn_Clear_Click(object sender, EventArgs e)
        {
            this.Clear();
        }

        private void Clear()
        {
            this.Controls.Clear();
            this.InitializeComponent();
            tabControl1.DrawItem += new DrawItemEventHandler(tabControl1_DrawItem);
            this.EnablePage(tabPage2, false);
            this.EnablePage(tabPage4, false);
            this.EnablePage(tabPage5, false);
            this.EnablePage(tabPage7, false);
            this.searchEnabled = false;
            this.LoadComboBoxes();
            this.SuccessfulSignIn(loginID);
            if (adminFileMenu == true)
            {
                this.EnableAdminFileMenu();
            }
            this.signOutToolStripMenuItem.Visible = true;
            this.currentUserToolStripMenuItem.Visible = true;
            this.currentUserToolStripMenuItem.Text = "Current User: " + loginID;
            this.signInToolStripMenuItem.Visible = false;
        }

        private void btn_CreateNewAppointment_Click(object sender, EventArgs e)
        {

            if (comboBox_PatientSelect.SelectedIndex == 0 || searchEnabled == false)
            {
                MessageBox.Show("Please search for and select a patient first.");
            }
            else
            {
                View_UpdateAppointment updateAppointment = new View_UpdateAppointment(this.selectedPatient.patientID);
                //updateAppointment.FormClosed += (s, args) => resetSearchResults();   Previously used to reload search
                updateAppointment.Show();
                this.getPatientAppointments(selectedPatient.patientID); //populates appointment drop down list
            }
        }

        private bool IsValidDataPatientUpdate()
        {
            if (Validator.IsPresent(txtBox_Result_FirstName) &&
                Validator.IsPresent(txtBox_Result_LastName) &&
                Validator.IsPresent(txtBox_Result_Gender) &&
                Validator.IsGender(txtBox_Result_Gender.Text.ToString()) &&
                Validator.IsPresent(txtBox_Result_DOB) &&
                Validator.IsDOB(txtBox_Result_DOB.Text.ToString()) &&
                Validator.IsPresent(txtBox_Result_SSN) &&
                Validator.IsSSN(txtBox_Result_SSN.Text.ToString()) &&
                Validator.IsPresent(txtBox_Result_Address1) &&
                Validator.IsPresent(txtBox_Result_City) &&
                Validator.IsPresent(comboBox_Result_State) &&
                Validator.IsPresent(txtBox_Result_Zip) &&
                Validator.IsZipCode(txtBox_Result_Zip.Text.ToString()) &&
                Validator.IsPresent(txtBox_Result_Phone) &&
                Validator.IsPhoneNumber(txtBox_Result_Phone.Text.ToString()))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void View_Main_Load(object sender, EventArgs e)
        {
            this.LoadComboBoxes();
            comboBox_Result_State.SelectedIndex = -1;
        }

        private void btn_UpdateAppointment_Click(object sender, EventArgs e)
        {
            selectedAppointment = appointmentList[comboBox_AppointmentSelect.SelectedIndex];
            if (selectedAppointment.ApptDate < DateTime.Today)
            {
                MessageBox.Show("Can't edit an appointment scheduled before today.");
            }
            else
            {
                View_UpdateAppointment newAppointment = new View_UpdateAppointment(this.selectedAppointment.ApptID, this.selectedPatient.patientID);
                newAppointment.FormClosed += (s, args) => this.getPatientAppointments(this.selectedPatient.patientID);
                newAppointment.Show();
            }
        }

        private void reportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new View_ReportTool().Show();
        }

        private void btn_CheckIn_Complete_Click(object sender, EventArgs e)
        {
            // need to know apptID
            int apptID = appointmentList[comboBox_AppointmentSelect.SelectedIndex].ApptID;
            // check to see if valid
            if(isValidVisit())
            {
                // try to add record for visit
                this.PutVisitData(visit);
                try
                {
                    this.inController.AddVisit(visit);
                    this.DialogResult = DialogResult.OK;
                    MessageBox.Show("Check-in completed successfully");
                    this.EnablePage( tabPage4, false);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
            this.btn_Search.PerformClick();
        }

        private bool isValidVisit()
        {
            string message = "";
            if (txtBox_CheckIn_FirstName.Text == "")
                message += "First name cannot be empty\n";
            if (txtBox_CheckIn_LastName.Text == "")
                message += "Last name cannot be empty\n";
            if ((txtBox_CheckIn_Pulse.Text == ""  || Int32.Parse(txtBox_CheckIn_Pulse.Text) <= 0))
                message += "Pulse must be greater than 0\n";
            if ((txtBox_CheckIn_Diastolic.Text == "" || Int32.Parse(txtBox_CheckIn_Diastolic.Text) <= 0))
                message += "Diastolic BP must be greater than 0\n";
            if ((txtBox_CheckIn_Systolic.Text == "" || Int32.Parse(txtBox_CheckIn_Systolic.Text) <= 0))
                message += "Systolic BP must be greater than 0\n";
            if ((txtBox_CheckIn_Temperature.Text == "" || Double.Parse(txtBox_CheckIn_Temperature.Text) <= 0))
                message += "Temperature must be greater than 0\n";
            if (txtBox_CheckIn_ReasonForAppointment.Text == "")
                message += "Reason for Appointment is required ";
            if (txtBox_CheckIn_Symptoms.Text == "")
                message += "Symptoms are required ";
            if (message != "")
                MessageBox.Show(message);
            return (message == "");
        }

        private void PutVisitData(Visit visit)
        {
            Nurse curr_nurse = new Nurse();
            curr_nurse = inController.GetNurse(loginID);
            visit.apptID = appointmentList[comboBox_AppointmentSelect.SelectedIndex].ApptID;
            visit.bpSystolic = Int32.Parse(txtBox_CheckIn_Systolic.Text);
            visit.bpDiastolic = Int32.Parse(txtBox_CheckIn_Diastolic.Text);
            visit.temperature = Double.Parse(txtBox_CheckIn_Temperature.Text);
            visit.pulse = Int32.Parse(txtBox_CheckIn_Pulse.Text);
            visit.nurseID = curr_nurse.NurseID;
            visit.symptoms = txtBox_CheckIn_Symptoms.Text;
            visit.diagnosis = "";
        }

        private void tabPage2_Enter(object sender, EventArgs e)
        {
            selectedAppointment = appointmentList[comboBox_AppointmentSelect.SelectedIndex];
            txtBox_CheckIn_Date.Text = selectedAppointment.ApptDate.ToShortDateString();
            txtBox_CheckIn_Time.Text = selectedAppointment.ApptTime;
            txtBox_CheckIn_ReasonForAppointment.Text = selectedAppointment.ReasonForVisit;
        }

        private void btn_CheckIn_Clear_Click(object sender, EventArgs e)
        {
            txtBox_CheckIn_Diastolic.Clear();
            txtBox_CheckIn_Systolic.Clear();
            txtBox_CheckIn_Temperature.Clear();
            txtBox_CheckIn_Pulse.Clear();
            txtBox_CheckIn_Symptoms.Clear();
            txtBox_CheckIn_Systolic.Focus();
        }

        private void getAvailableTests()
        {
            this.testList = this.inController.GetAvailableTests();
        }

        private void btn_Order_Test_Click(object sender, EventArgs e)
        {
            if (visit.diagnosis.Length > 0)
            {
                btn_Order_Test.Visible = false;
                comboBox_Lab_Test.Visible = false;
                label18.Visible = false;
            }
            else
            {
                test = testList[comboBox_Lab_Test.SelectedIndex];
                test.VisitID = visit.visitID;
                try
                {
                    inController.OrderTest(test);
                    MessageBox.Show(test.TestName + " has been ordered successfully");
                }
                catch
                {
                    MessageBox.Show("Unable to order test");
                }
                PopulateTestsOrdered(test.VisitID);
            }            
        }

        public void PopulateTestsOrdered(int visitID)
        {
            lstBox_Lab_TestsOrdered.DataSource = inController.GetTests(visitID).ToList();
            lstBox_Lab_TestsOrdered.DisplayMember = "TestFullName";
            lstBox_Lab_TestsOrdered.ValueMember = "TestPerformedID";
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {
        }

        private void loadCheckOut()
        {
            txtBox_CheckOut_LastName.Text = selectedPatient.lname;
            txtBox_CheckOut_FirstName.Text = selectedPatient.fname;
            txtBox_CheckOut_ReasonForVisit.Text = selectedAppointment.ReasonForVisit;
            txtBox_CheckOut_SystolicBP.Text = visit.bpSystolic.ToString();
            txtBox_CheckOut_DiastolicBP.Text = visit.bpDiastolic.ToString();
            txtBox_CheckOut_Temperature.Text = visit.temperature.ToString();
            txtBox_CheckOut_Pulse.Text = visit.pulse.ToString();
            txtBox_CheckOut_Symptoms.Text = visit.symptoms;
        }

        private void btn_CheckOut_Complete_Click(object sender, EventArgs e)
        {
            if(txtBox_CheckOut_Diagnosis.Text == "")
            {
                MessageBox.Show("Please enter a diagnosis");
            }
            else
            {
                try
                {
                    visit = inController.GetPatientVisit(selectedAppointment.ApptID);
                    visit.diagnosis = txtBox_CheckOut_Diagnosis.Text;
                    inController.CompleteCheckOut(visit);
                    this.DialogResult = DialogResult.OK;
                    MessageBox.Show("Check-out completed successfully");
                    btn_Order_Test.Visible = false;
                    comboBox_Lab_Test.Visible = false;
                    label18.Visible = false;
                    this.EnablePage(tabPage7, false);
                    this.getPatientAppointments(selectedPatient.patientID);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
            this.btn_Search.PerformClick();
        }

        private void btn_Lab_EnterResult_Click(object sender, EventArgs e)
        {
            if(lstBox_Lab_TestsOrdered.SelectedIndex != -1)
            {
                int selected = (int)lstBox_Lab_TestsOrdered.SelectedValue;
                Test selectedTest = inController.GetTest(selected);
                try
                {
                    View_UpdateTest updateTestForm = new View_UpdateTest(selected, selectedPatient.patientID, selectedAppointment.ApptID);
                    updateTestForm.ShowDialog();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
            else
            {
                MessageBox.Show("Please select a test");
            }
        }
    }
}
