﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Group4_Project.Controller;
using Group4_Project.Model;

namespace Group4_Project.View
{
    public partial class View_NewPatient : Form
    {
        private Group4Controller inController;
        private List<State> stateList;
        private Patient patient;

        public View_NewPatient()
        {
            InitializeComponent();
            inController = new Group4Controller();
        }

        private void LoadComboBoxes()
        {
            try
            {
                stateList = this.inController.GetStateList();
                comboBox_State.DataSource = stateList;
                comboBox_State.DisplayMember = "StateID";
                comboBox_State.ValueMember = "StateID";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private bool IsValidData()
        {
            if (Validator.IsPresent(txtBox_FirstName) && 
                Validator.IsPresent(txtBox_LastName) && 
                Validator.IsPresent(txtBox_Gender) &&
                Validator.IsGender(txtBox_Gender.Text.ToString()) && 
                Validator.IsPresent(txtBox_DoB) && 
                Validator.IsDOB(txtBox_DoB.Text.ToString()) && 
                Validator.IsPresent(txtBox_SSN) &&
                Validator.IsSSN(txtBox_SSN.Text.ToString()) && 
                Validator.IsPresent(txtBox_Address1) &&
                Validator.IsPresent(txtBox_City) && 
                Validator.IsPresent(comboBox_State) && 
                Validator.IsPresent(txtBox_Zip) &&
                Validator.IsZipCode(txtBox_Zip.Text.ToString()) &&  
                Validator.IsPresent(txtBox_Phone) && 
                Validator.IsPhoneNumber(txtBox_Phone.Text.ToString()))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btn_AddPatient_Click(object sender, EventArgs e)
        {
            if (IsValidData())
            {
                patient = new Patient();
                this.PutPatientData(patient);
                try
                {
                    this.inController.AddPatient(patient);
                    this.DialogResult = DialogResult.OK;
                    MessageBox.Show("Patient added successfully.");
                    Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().ToString());
                }
            }
        }

        private void PutPatientData(Patient patient)
        {
            patient.fname = txtBox_FirstName.Text;
            patient.lname = txtBox_LastName.Text;
            patient.gender = txtBox_Gender.Text;
            patient.dob = Convert.ToDateTime(txtBox_DoB.Text);
            patient.ssn = txtBox_SSN.Text;
            patient.address = txtBox_Address1.Text;
            patient.city = txtBox_City.Text;
            patient.state = comboBox_State.SelectedValue.ToString();
            patient.zip = txtBox_Zip.Text;
            patient.phone = txtBox_Phone.Text;
        }

        private void View_NewPatient_Load_1(object sender, EventArgs e)
        {
            this.LoadComboBoxes();
            comboBox_State.SelectedIndex = -1;
        }
    }
}
