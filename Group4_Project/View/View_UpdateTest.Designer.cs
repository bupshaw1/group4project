﻿namespace Group4_Project.View
{
    partial class View_UpdateTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl_UpdTest_Name = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbl_UpdTest_ApptDate = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lbl_UpdTest_Patient = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_UpdTest_EnterResults = new System.Windows.Forms.Button();
            this.dat_UpdTest_DatePerformed = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.rad_Abnormal = new System.Windows.Forms.RadioButton();
            this.rad_Normal = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(101, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Test Details";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Test Name:";
            // 
            // lbl_UpdTest_Name
            // 
            this.lbl_UpdTest_Name.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_UpdTest_Name.Location = new System.Drawing.Point(106, 84);
            this.lbl_UpdTest_Name.Name = "lbl_UpdTest_Name";
            this.lbl_UpdTest_Name.Size = new System.Drawing.Size(165, 23);
            this.lbl_UpdTest_Name.TabIndex = 2;
            this.lbl_UpdTest_Name.Text = "label3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 119);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Appt Date:";
            // 
            // lbl_UpdTest_ApptDate
            // 
            this.lbl_UpdTest_ApptDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_UpdTest_ApptDate.Location = new System.Drawing.Point(106, 118);
            this.lbl_UpdTest_ApptDate.Name = "lbl_UpdTest_ApptDate";
            this.lbl_UpdTest_ApptDate.Size = new System.Drawing.Size(165, 23);
            this.lbl_UpdTest_ApptDate.TabIndex = 7;
            this.lbl_UpdTest_ApptDate.Text = "label3";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 51);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Patient:";
            // 
            // lbl_UpdTest_Patient
            // 
            this.lbl_UpdTest_Patient.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_UpdTest_Patient.Location = new System.Drawing.Point(106, 50);
            this.lbl_UpdTest_Patient.Name = "lbl_UpdTest_Patient";
            this.lbl_UpdTest_Patient.Size = new System.Drawing.Size(165, 23);
            this.lbl_UpdTest_Patient.TabIndex = 9;
            this.lbl_UpdTest_Patient.Text = "label3";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_UpdTest_EnterResults);
            this.groupBox1.Controls.Add(this.dat_UpdTest_DatePerformed);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.rad_Abnormal);
            this.groupBox1.Controls.Add(this.rad_Normal);
            this.groupBox1.Location = new System.Drawing.Point(17, 183);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(254, 167);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Enter Test Results:";
            // 
            // btn_UpdTest_EnterResults
            // 
            this.btn_UpdTest_EnterResults.Location = new System.Drawing.Point(87, 128);
            this.btn_UpdTest_EnterResults.Name = "btn_UpdTest_EnterResults";
            this.btn_UpdTest_EnterResults.Size = new System.Drawing.Size(98, 23);
            this.btn_UpdTest_EnterResults.TabIndex = 13;
            this.btn_UpdTest_EnterResults.Text = "Save Results";
            this.btn_UpdTest_EnterResults.UseVisualStyleBackColor = true;
            this.btn_UpdTest_EnterResults.Click += new System.EventHandler(this.btn_UpdTest_EnterResults_Click);
            // 
            // dat_UpdTest_DatePerformed
            // 
            this.dat_UpdTest_DatePerformed.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dat_UpdTest_DatePerformed.Location = new System.Drawing.Point(114, 31);
            this.dat_UpdTest_DatePerformed.Name = "dat_UpdTest_DatePerformed";
            this.dat_UpdTest_DatePerformed.Size = new System.Drawing.Size(134, 20);
            this.dat_UpdTest_DatePerformed.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Date Performed:";
            // 
            // rad_Abnormal
            // 
            this.rad_Abnormal.AutoSize = true;
            this.rad_Abnormal.Location = new System.Drawing.Point(89, 95);
            this.rad_Abnormal.Name = "rad_Abnormal";
            this.rad_Abnormal.Size = new System.Drawing.Size(69, 17);
            this.rad_Abnormal.TabIndex = 1;
            this.rad_Abnormal.TabStop = true;
            this.rad_Abnormal.Text = "Abnormal";
            this.rad_Abnormal.UseVisualStyleBackColor = true;
            // 
            // rad_Normal
            // 
            this.rad_Normal.AutoSize = true;
            this.rad_Normal.Location = new System.Drawing.Point(89, 72);
            this.rad_Normal.Name = "rad_Normal";
            this.rad_Normal.Size = new System.Drawing.Size(58, 17);
            this.rad_Normal.TabIndex = 0;
            this.rad_Normal.TabStop = true;
            this.rad_Normal.Text = "Normal";
            this.rad_Normal.UseVisualStyleBackColor = true;
            // 
            // View_UpdateTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(315, 396);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lbl_UpdTest_Patient);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lbl_UpdTest_ApptDate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbl_UpdTest_Name);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "View_UpdateTest";
            this.Text = "Test";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl_UpdTest_Name;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbl_UpdTest_ApptDate;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbl_UpdTest_Patient;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rad_Abnormal;
        private System.Windows.Forms.RadioButton rad_Normal;
        private System.Windows.Forms.Button btn_UpdTest_EnterResults;
        private System.Windows.Forms.DateTimePicker dat_UpdTest_DatePerformed;
        private System.Windows.Forms.Label label3;
    }
}