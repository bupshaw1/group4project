﻿namespace Group4_Project.View
{
    partial class View_ReportTool
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this._CS6232_g4DataSet = new Group4_Project._CS6232_g4DataSet();
            this.spmostPerformedTestsDuringDatesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp_mostPerformedTestsDuringDatesTableAdapter = new Group4_Project._CS6232_g4DataSetTableAdapters.sp_mostPerformedTestsDuringDatesTableAdapter();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBox_Start_Date = new System.Windows.Forms.TextBox();
            this.txtBox_End_Date = new System.Windows.Forms.TextBox();
            this.btn_Run_Report = new System.Windows.Forms.Button();
            this.sp_mostPerformedTestsDuringDatesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._CS6232_g4DataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spmostPerformedTestsDuringDatesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_mostPerformedTestsDuringDatesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // reportViewer1
            // 
            reportDataSource1.Name = "AdminReportDS";
            reportDataSource1.Value = this.sp_mostPerformedTestsDuringDatesBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "Group4_Project.View.Admin_Report.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(12, 117);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(903, 284);
            this.reportViewer1.TabIndex = 0;
            // 
            // _CS6232_g4DataSet
            // 
            this._CS6232_g4DataSet.DataSetName = "_CS6232_g4DataSet";
            this._CS6232_g4DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // spmostPerformedTestsDuringDatesBindingSource
            // 
            this.spmostPerformedTestsDuringDatesBindingSource.DataMember = "sp_mostPerformedTestsDuringDates";
            this.spmostPerformedTestsDuringDatesBindingSource.DataSource = this._CS6232_g4DataSet;
            // 
            // sp_mostPerformedTestsDuringDatesTableAdapter
            // 
            this.sp_mostPerformedTestsDuringDatesTableAdapter.ClearBeforeFill = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Start Date:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "End Date:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // txtBox_Start_Date
            // 
            this.txtBox_Start_Date.Location = new System.Drawing.Point(78, 13);
            this.txtBox_Start_Date.Name = "txtBox_Start_Date";
            this.txtBox_Start_Date.Size = new System.Drawing.Size(100, 20);
            this.txtBox_Start_Date.TabIndex = 3;
            // 
            // txtBox_End_Date
            // 
            this.txtBox_End_Date.Location = new System.Drawing.Point(78, 44);
            this.txtBox_End_Date.Name = "txtBox_End_Date";
            this.txtBox_End_Date.Size = new System.Drawing.Size(100, 20);
            this.txtBox_End_Date.TabIndex = 4;
            // 
            // btn_Run_Report
            // 
            this.btn_Run_Report.Location = new System.Drawing.Point(78, 71);
            this.btn_Run_Report.Name = "btn_Run_Report";
            this.btn_Run_Report.Size = new System.Drawing.Size(75, 23);
            this.btn_Run_Report.TabIndex = 5;
            this.btn_Run_Report.Text = "Run Report";
            this.btn_Run_Report.UseVisualStyleBackColor = true;
            this.btn_Run_Report.Click += new System.EventHandler(this.btn_Run_Report_Click);
            // 
            // sp_mostPerformedTestsDuringDatesBindingSource
            // 
            this.sp_mostPerformedTestsDuringDatesBindingSource.DataMember = "sp_mostPerformedTestsDuringDates";
            this.sp_mostPerformedTestsDuringDatesBindingSource.DataSource = this._CS6232_g4DataSet;
            // 
            // View_ReportTool
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(927, 413);
            this.Controls.Add(this.btn_Run_Report);
            this.Controls.Add(this.txtBox_End_Date);
            this.Controls.Add(this.txtBox_Start_Date);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.reportViewer1);
            this.Name = "View_ReportTool";
            this.Text = "View_ReportTool";
            this.Load += new System.EventHandler(this.View_ReportTool_Load);
            ((System.ComponentModel.ISupportInitialize)(this._CS6232_g4DataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spmostPerformedTestsDuringDatesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_mostPerformedTestsDuringDatesBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource spmostPerformedTestsDuringDatesBindingSource;
        private _CS6232_g4DataSet _CS6232_g4DataSet;
        private _CS6232_g4DataSetTableAdapters.sp_mostPerformedTestsDuringDatesTableAdapter sp_mostPerformedTestsDuringDatesTableAdapter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBox_Start_Date;
        private System.Windows.Forms.TextBox txtBox_End_Date;
        private System.Windows.Forms.Button btn_Run_Report;
        private System.Windows.Forms.BindingSource sp_mostPerformedTestsDuringDatesBindingSource;
    }
}