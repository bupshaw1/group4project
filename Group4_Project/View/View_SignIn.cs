﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Group4_Project.View;
using Group4_Project.Controller;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.IO;

namespace Group4_Project.View
{
    public partial class View_SignIn : Form
    {
        private Group4Controller inController;
        private string current_user = "";

        public View_SignIn()
        {
            InitializeComponent();
            inController = new Group4Controller();
            txtbox_Password.PasswordChar = '*';
        }

        private void btn_OK_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.isValidSignIn(txtbox_Username.Text, Encrypt(txtbox_Password.Text.ToString())) && this.isValidAdmin(txtbox_Username.Text, Encrypt(txtbox_Password.Text.ToString())) && this.isValidNurse(txtbox_Username.Text, Encrypt(txtbox_Password.Text.ToString())))
                {

                    current_user = txtbox_Username.Text.ToString();
                    (System.Windows.Forms.Application.OpenForms["View_Main"] as View_Main).EnableTab1();
                    (System.Windows.Forms.Application.OpenForms["View_Main"] as View_Main).EnableAdminFileMenu();
                    (System.Windows.Forms.Application.OpenForms["View_Main"] as View_Main).SuccessfulSignIn(current_user);
                    Close();
                }
                else if (this.isValidSignIn(txtbox_Username.Text, Encrypt(txtbox_Password.Text.ToString())) && this.isValidAdmin(txtbox_Username.Text, Encrypt(txtbox_Password.Text.ToString())))
                {
                    current_user = txtbox_Username.Text.ToString();
                    (System.Windows.Forms.Application.OpenForms["View_Main"] as View_Main).EnableAdminFileMenu();
                    (System.Windows.Forms.Application.OpenForms["View_Main"] as View_Main).SuccessfulSignIn(current_user);
                    Close();
                }
                else if (this.isValidSignIn(txtbox_Username.Text, Encrypt(txtbox_Password.Text.ToString())) && this.isValidNurse(txtbox_Username.Text, Encrypt(txtbox_Password.Text.ToString())))
                {
                    current_user = txtbox_Username.Text.ToString();
                    (System.Windows.Forms.Application.OpenForms["View_Main"] as View_Main).EnableTab1();
                    (System.Windows.Forms.Application.OpenForms["View_Main"] as View_Main).SuccessfulSignIn(current_user);
                    Close();
                }
                else if (this.isValidSignIn(txtbox_Username.Text, Encrypt(txtbox_Password.Text.ToString())))
                {
                    current_user = txtbox_Username.Text.ToString();
                    (System.Windows.Forms.Application.OpenForms["View_Main"] as View_Main).SuccessfulSignIn(current_user);
                    Close();
                }
                else if (txtbox_Username.Text == "" && txtbox_Password.Text == "")
                {
                    MessageBox.Show("Username and Password are required.");
                }
                else if (txtbox_Username.Text == "")
                {
                    MessageBox.Show("Username is required.");
                }
                else if (txtbox_Password.Text == "")
                {
                    MessageBox.Show("Password is required.");
                }
                else
                {
                    MessageBox.Show("Invalid username/password combination.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private Boolean isValidSignIn(string username, string password)
        {
            if (this.inController.GetProgUser(username, password) == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool isValidAdmin(string username, string password)
        {
            if (this.inController.GetAdminUser(username, password) == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool isValidNurse(string username, string password)
        {
            if (this.inController.GetNurseUser(username, password) == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }
    }
}
