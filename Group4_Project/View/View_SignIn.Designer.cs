﻿namespace Group4_Project.View
{
    partial class View_SignIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtbox_Username = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtbox_Password = new System.Windows.Forms.TextBox();
            this.btn_Sign_In = new System.Windows.Forms.Button();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 36);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Username :";
            // 
            // txtbox_Username
            // 
            this.txtbox_Username.Location = new System.Drawing.Point(104, 36);
            this.txtbox_Username.Margin = new System.Windows.Forms.Padding(2);
            this.txtbox_Username.Name = "txtbox_Username";
            this.txtbox_Username.Size = new System.Drawing.Size(186, 20);
            this.txtbox_Username.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(38, 65);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Password :";
            // 
            // txtbox_Password
            // 
            this.txtbox_Password.AcceptsReturn = true;
            this.txtbox_Password.AcceptsTab = true;
            this.txtbox_Password.Location = new System.Drawing.Point(104, 65);
            this.txtbox_Password.Margin = new System.Windows.Forms.Padding(2);
            this.txtbox_Password.Name = "txtbox_Password";
            this.txtbox_Password.Size = new System.Drawing.Size(186, 20);
            this.txtbox_Password.TabIndex = 3;
            // 
            // btn_Sign_In
            // 
            this.btn_Sign_In.Location = new System.Drawing.Point(104, 120);
            this.btn_Sign_In.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Sign_In.Name = "btn_Sign_In";
            this.btn_Sign_In.Size = new System.Drawing.Size(56, 24);
            this.btn_Sign_In.TabIndex = 4;
            this.btn_Sign_In.Text = "Sign In";
            this.btn_Sign_In.UseVisualStyleBackColor = true;
            this.btn_Sign_In.Click += new System.EventHandler(this.btn_OK_Click);
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(233, 120);
            this.btn_Cancel.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(56, 24);
            this.btn_Cancel.TabIndex = 5;
            this.btn_Cancel.Text = "Cancel";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.button1_Click);
            // 
            // View_SignIn
            // 
            this.AcceptButton = this.btn_Sign_In;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(369, 206);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_Sign_In);
            this.Controls.Add(this.txtbox_Password);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtbox_Username);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "View_SignIn";
            this.Text = "Sign In";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtbox_Username;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtbox_Password;
        private System.Windows.Forms.Button btn_Sign_In;
        private System.Windows.Forms.Button btn_Cancel;
    }
}