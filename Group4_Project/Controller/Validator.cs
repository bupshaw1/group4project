﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Group4_Project
{
    class Validator
    {
        private static string title = "Entry Error";

        public static string Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
            }
        }

        public static bool IsPresent(Control control)
        {
            if (control.GetType().ToString() == "System.Windows.Forms.TextBox")
            {
                TextBox textBox = (TextBox)control;
                if (textBox.Text == "")
                {
                    MessageBox.Show("All fields are required.");
                    textBox.Focus();
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else if (control.GetType().ToString() == "System.Windows.Forms.ComboBox")
            {
                ComboBox comboBox = (ComboBox)control;
                if (comboBox.SelectedIndex == -1)
                {
                    MessageBox.Show("All fields are required.");
                    comboBox.Focus();
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return true;
        }

        public static bool IsDecimal(TextBox textBox)
        {
            try
            {
                Convert.ToDecimal(textBox.Text);
                return true;
            }
            catch (FormatException)
            {
                MessageBox.Show(textBox.Tag.ToString() + " must be a decimal value.", Title);
                textBox.Focus();
                return false;
            }
        }

        public static bool IsInt32(TextBox textBox)
        {
            try
            {
                Convert.ToInt32(textBox.Text);
                return true;
            }
            catch (FormatException)
            {
                MessageBox.Show(textBox.Tag.ToString() + " must be an integer value.", Title);
                textBox.Focus();
                return false;
            }
        }

        public static bool IsWithinRange(TextBox textBox, decimal min, decimal max)
        {
            decimal dNumber = Convert.ToDecimal(textBox.Text);
            if (dNumber < min || dNumber > max)
            {
                MessageBox.Show(textBox.Tag.ToString() + " must be between " +
                    min + " and " + max + ".", Title);
                textBox.Focus();
                return false;
            }
            else
            {
                return true;
            }
        }

        public static bool IsZipCode(string zipCode)
        {
            string _usZipRegEx = @"^\d{5}(?:[-\s]\d{4})?$";
            bool validZipCode = true;
            if ((!Regex.Match(zipCode, _usZipRegEx).Success))
            {
                MessageBox.Show("Zip code must be a valid five ('12345') or nine ('12345-1234') format.");
                validZipCode = false;
            }
            return validZipCode;
        }

        public static bool IsSSN(string ssn)
        {
            bool validSSN = true;
            string _ssnRegEx = @"^\d{9}$";
            if ((!Regex.Match(ssn, _ssnRegEx).Success))
            {
                MessageBox.Show("SSN must be a valid nine digit number with no hyphens ('123456789').");
                validSSN = false;
            }
            return validSSN;
        }

        public static bool IsPhoneNumber(string phone)
        {
            bool validPhoneNumber = true;
            string _phoneNumberRegEx = @"^\(\d{3}\)\s\d{3}-\d{4}$";
            if ((!Regex.Match(phone, _phoneNumberRegEx).Success))
            {
                MessageBox.Show("Phone number must be in the following format: (XXX) XXX-XXXX");
                validPhoneNumber = false;
            }
            return validPhoneNumber;
        }

        public static bool IsGender(string gender)
        {
            bool validGender = true;
            string _genderRegEx = @"^[mMfF]$";
            if ((!Regex.Match(gender, _genderRegEx).Success))
            {
                MessageBox.Show("Gender must be either M or F.");
                validGender = false;
            }
            return validGender;
        }

        public static bool IsDOB(string dob)
        {
            bool validDOB = true;
            string _dobRegEx = @"^\d{4}-\d{2}-\d{2}$";
            if ((!Regex.Match(dob, _dobRegEx).Success))
            {
                MessageBox.Show("DOB must be in a valid YYYY-MM-DD format (ex. 1900-01-01).");
                validDOB = false;
            }
            return validDOB;
        }

        public static bool IsDate(string date)
        {
            bool validDate = true;
            string _dateRegEx = @"^\d{4}-\d{2}-\d{2}$";
            if ((!Regex.Match(date, _dateRegEx).Success))
            {
                MessageBox.Show("Date must be in a valid YYYY-MM-DD format (ex. 1900-01-01).");
                validDate = false;
            }
            return validDate;
        }

        public static bool IsStartDateBeforeEndDate(string startDate, string endDate)
        {
            bool validDate = true;
            DateTime startDate1 = Convert.ToDateTime(startDate);
            DateTime endDate1 = Convert.ToDateTime(endDate);
            if (startDate1 > endDate1)
            {
                MessageBox.Show("The start date must be earlier than the end date.");
                validDate = false;
            }
            return validDate;
        }
    }
}
