﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Group4_Project.DAL;
using Group4_Project.Model;

namespace Group4_Project.Controller
{
    class Group4Controller
    {
        public Group4Controller()
        {

        }

        public Prog_User GetProgUser(string login, string pass)
        {
            return Prog_UserDB.GetProgUser(login, pass);
        }

        public Nurse GetNurse(string loginID)
        {
            return NurseDB.GetNurse(loginID);
        }

        public Admin_User GetAdminUser(string login, string pass)
        {
            return Admin_UserDB.GetAdminUser(login, pass);
        }

        public Nurse GetNurseUser(string login, string pass)
        {
            return NurseDB.GetNurseUser(login, pass);
        }

        public List<Patient> SearchByName(string lastName, string firstName)
        {
            return PatientDB.SearchByName(lastName, firstName);
        }

        public List<Patient> SearchByLastAndDOB(string lastName, DateTime dob)
        {
            return PatientDB.SearchByLastAndDOB(lastName, dob);
        }

        public List<Patient> SearchByDOB(DateTime dob)
        {
            return PatientDB.SearchByDOB(dob);
        }

        public List<Doctor> GetDoctorList()
        {
            return DoctorDB.GetDoctorList();
        }

        public List<Test> GetAvailableTests()
        {
            return TestDB.GetAvailableTests();
        }
 
        public bool AddPatient(Patient patient)
        {
            return PatientDB.AddPatient(patient);
        }

        public bool AddAppointment(Appointment appointment)
        {
            return AppointmentDB.AddAppointment(appointment);
        }

        public bool OrderTest(Test test)
        {
            return TestDB.OrderTest(test);
        }

        public bool UpdateAppointment(Appointment appointment)
        {
            return AppointmentDB.UpdateAppointment(appointment);
        }

        public bool EnterTestResults(Test updateTest)
        {
            return TestDB.EnterTestResults(updateTest);
        }

        public List<Test> GetTests(int visitID)
        {
            return TestDB.GetTests(visitID);
        }

        public Test GetTest(int testPerformedID)
        {
            return TestDB.GetTest(testPerformedID);
        }

        public bool AddVisit(Visit visit)
        {
            return VisitDB.AddVisit(visit);
        }

        public Patient GetPatient(int patientID)
        {
            return PatientDB.GetPatient(patientID);
        }

        public Appointment GetAppointment(int appointmentID)
        {
            return AppointmentDB.GetAppointment(appointmentID);
        }

        public List<State> GetStateList()
        {
            return StateDB.GetStateList();
        }
        public List<Appointment> GetPatientAppointments(int patientID)
        {
            return AppointmentDB.GetAppointments(patientID);
        }

        public Visit GetPatientVisit(int apptID)
        {
            return VisitDB.GetVisit(apptID);
        }

        public bool CompleteCheckOut(Visit visit)
        {
            return VisitDB.CompleteVisit(visit);
        }
    }
}
