﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group4_Project.Model
{
    class Doctor
    {
        public string fname { get; set; }
        public string lname { get; set; }
        public int doctorID { get; set; }
        public int personID { get; set; }
        public int drSpecialtyID { get; set; }
        public string specialtyName { get; set; }
        public string displayFullName { get; set; }
    }
}
