﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group4_Project.Model
{
    class Appointment
    {
        public int ApptID { get; set; }
        public DateTime ApptDate { get; set; }
        public string ApptTime { get; set; }
        public string ReasonForVisit { get; set; }
        public int PatientID { get; set; }
        public int DoctorID { get; set; }
        public string DoctorFullName { get; set; }
        public string DisplayFullAppt { get; set; }
    }
}
