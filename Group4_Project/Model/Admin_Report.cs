﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group4_Project.Model
{
    class Admin_Report
    {
        public int TestCode { get; set; }
        public string TestName { get; set; }
        public int TotalTimesPerformed { get; set; }
        public int AllTestsTotal { get; set; }
        public decimal PercentOfTotal { get; set; }
        public int NormalTestResults { get; set; }
        public int AbnormalTestResults { get; set; }
        public decimal PatientsBetween18And29 { get; set; }
        public decimal PatientsOutside18And29 { get; set; }
    }
}
