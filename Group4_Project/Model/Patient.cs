﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group4_Project.Model
{
    class Patient
    {
        public string fname { get; set; }
        public string lname { get; set; }
        public DateTime dob { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string phone { get; set; }
        public string gender { get; set; }
        public string ssn { get; set; }
        public int personID { get; set; }
        public string displayFullName { get; set; }
        public int patientID { get; set; }

    }
}
