﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group4_Project.Model
{
    public class State
    {
        public string StateName { get; set; }
        public string StateID { get; set; }
    }
}
