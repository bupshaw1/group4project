﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group4_Project.Model
{
    public class Admin_User
    {
        public string adminID { get; set; }
        public string personID { get; set; }
        public string userID { get; set; }
    }
}
