﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group4_Project.Model
{
    class Visit
    {
        public int visitID { get; set; }
        public int apptID { get; set; }
        public int bpSystolic { get; set; }
        public int bpDiastolic { get; set; }
        public double temperature { get; set; }
        public int pulse { get; set; }
        public int nurseID { get; set; }
        public string symptoms { get; set; }
        public string diagnosis { get; set; }
    }
}
