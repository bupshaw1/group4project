﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group4_Project.Model
{
    class Test
    {
        public int TestPerformedID { get; set; }
        public int VisitID { get; set; }
        public int TestID { get; set; }
        public bool ResultsNormal { get; set; }
        public DateTime DatePerformed { get; set; }
        public string TestName { get; set; }
        public string TestFullName { get; set; }

    }
}
