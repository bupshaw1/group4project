﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Group4_Project.Model
{
    class Nurse
    {
        public int NurseID { get; set; }
        public int PersonID { get; set; }
        public int UserID { get; set; }
    }
}
